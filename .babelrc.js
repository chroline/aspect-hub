module.exports = (api) => ({
  presets: [
    [
      "@babel/preset-env",
      {
        useBuiltIns: "entry",
        corejs: 3,
      },
    ],
    "@babel/preset-typescript",
    "@babel/react",
  ],
  plugins: [
    "@babel/plugin-transform-react-jsx",
    "macros",
    "console",
    [
      "@babel/plugin-transform-typescript",
      {
        allowNamespaces: true,
      },
    ],
    [
      "styled-jsx/babel",
      {
        plugins: [
          [
            "styled-jsx-plugin-postcss",
            {
              path: "[PATH_PREFIX]/postcss.config.js",
            },
          ],
        ],
      },
    ],
    "emotion",
    "transform-optional-chaining",
    "@babel/plugin-proposal-export-namespace-from",
    "@babel/plugin-syntax-dynamic-import",
    "@babel/plugin-transform-runtime",
    [
      "@babel/plugin-proposal-decorators",
      {
        legacy: true,
      },
    ],
    [
      "@babel/plugin-proposal-class-properties",
      {
        loose: true,
      },
    ],
    !api.env("production") && "react-refresh/babel",
  ].filter((v) => v),
});
