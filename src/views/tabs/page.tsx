import withTheme from "structure/withTheme";
import { motion } from "framer-motion";
import { css } from "@emotion/core";
import { transition } from "util/theme/constants";
import React from "react";
import { useHistory } from "react-router";

const PageWrapper = withTheme(
  () => css`
    padding-bottom: calc(56px + env(safe-area-inset-bottom));
    width: 100%;
  `
)(motion.div);

const pageVariants = {
  initial: {
    opacity: 0,
  },
  in: {
    opacity: 1,
    scale: 1,
    position: "relative",
    transition: {
      ...transition,
      delay: 0.2,
    },
  },
  out: {
    opacity: 0,
    position: "fixed",
    transition,
  },
};

const Page: React.FC<{ className: string }> = ({ children, className }) => {
  const history = useHistory();
  return (
    <PageWrapper
      animate={"in"}
      initial={history.action === "REPLACE" ? "in" : "initial"}
      exit={"out"}
      variants={pageVariants}
      className={className}
      role={"main"}
    >
      {children}
    </PageWrapper>
  );
};

export default withTheme(() => css``)(Page);
