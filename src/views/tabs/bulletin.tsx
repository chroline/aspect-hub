import React, { useEffect, useState } from "react";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { useGroupData } from "store/GroupData";
import Page from "./page";
import Container from "components/atoms/Container";
import { useLockBodyScroll, useMount, useToggle, useUnmount } from "react-use";
import NewPostDirectory from "views/newPost/directory";
import { AnimatePresence, motion } from "framer-motion";
import Header from "components/molecules/Header";
import FAB from "components/atoms/FAB";
import { useHistory, useLocation } from "react-router";
import { BulletinPostsPaginator } from "store/Paginators";
import Firebase from "store/Firebase";
import { catchError, switchMap, take, takeWhile } from "rxjs/operators";
import { of } from "rxjs";
import Post from "util/interfaces/Post";
import { Button } from "components/atoms";
import { Edit3 } from "react-feather";
import PostsRenderer from "components/organisms/PostsRenderer";
import { transition } from "util/theme/constants";
import isError from "util/lambda/isError";
import iife from "util/iife";

const List = withTheme(
  ({ spacings }) => css`
    display: flex;
    justify-content: center;
    padding: ${spacings[12]} ${spacings[6]} 0 ${spacings[6]};
    width: 100%;
  `
)("div");

const animationVariants = {
  hidden: {
    opacity: 0,
  },
  show: {
    opacity: 1,
    transition: {
      staggerChildren: 0.2,
    },
  },
};

let paginator: BulletinPostsPaginator<Post>;
let groupID: string;

const Bulletin: React.FC<{ scroll: number }> = ({ scroll }) => {
  const [groupData] = useGroupData();
  const [toggle, setToggle] = useToggle(false);
  const history = useHistory();
  const location = useLocation();
  const [more, setMore] = useState(false);

  useEffect(() => {
    if (location.hash === "#new") {
      if (!location.state) {
        history.replace("/bulletin");
        history.push("/bulletin#new", true);
        setToggle(true);
      } else setToggle(true);
    } else setToggle(false);
  }, [location.hash]);

  const [active, setActive] = useState(true);
  const [posts, setPosts] = useState<Post[]>([]);

  useMount(() => setActive(true));
  useUnmount(() => setActive(false));

  useEffect(() => {
    paginator =
      groupID === groupData.id
        ? paginator
        : iife(() => {
            groupID = groupData.id;
            return new BulletinPostsPaginator<Post>(
              Firebase.app.firestore(),
              `groups/${groupData.id}/posts`,
              25
            );
          });
    let subscriber = paginator?.subject
      .pipe(
        takeWhile(() => active),
        catchError((err) => of(err as Error)),
        switchMap((val) =>
          of(
            isError(val as Error)
              ? val
              : (val as firebase.firestore.QueryDocumentSnapshot<Post>[]).map(
                  (value) => ({
                    id: value.id,
                    ...value.data(),
                  })
                )
          )
        )
      )
      .subscribe((value) => {
        if (!isError(value as Error)) {
          if (active) {
            setPosts(value as any);
            paginator.amnt.pipe(take(1)).subscribe((amnt) => {
              if (
                (value as firebase.firestore.QueryDocumentSnapshot<Post>[])
                  .length < amnt ||
                0
              )
                setMore(true);
              else setMore(false);
            });
          } else subscriber?.unsubscribe();
        }
      });
    return () => subscriber?.unsubscribe();
  }, [groupData.id]);

  useLockBodyScroll(toggle);

  const newPost = (type: string) => () => {
    switch (type) {
      case "text":
        history.push("/new/text", true);
        break;
      case "announcement":
        history.push("/new/announcement", true);
        break;
      case "media":
        history.push("/new/media", true);
        break;
    }
  };

  return (
    <>
      <Page
        css={css`
          background: white;
          min-height: 100%;
        `}
      >
        <Header
          css={css`
            border-bottom: 1px solid #dbdbdb;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.02),
              0 2px 1px rgba(0, 0, 0, 0.012), 0 1px 1px rgba(0, 0, 0, 0.014);
          `}
        >
          <Container
            css={css`
              position: relative;
            `}
          >
            <h1>What’s New?</h1>
            <h2>{groupData.name}</h2>
            <FAB.Normal
              scroll={scroll}
              onClick={() => history.push("/bulletin#new", true)}
            >
              <Edit3 />
            </FAB.Normal>
          </Container>
        </Header>
        <List>
          <Container
            size={"sm"}
            css={css`
              margin-bottom: 3.5rem;
            `}
          >
            <motion.div
              style={{ width: "100%" }}
              variants={animationVariants}
              initial={"hidden"}
              animate={"show"}
              key={"posts"}
            >
              <PostsRenderer posts={posts} />
              {more && (
                <motion.div positionTransition={transition}>
                  <Button
                    color={"primary"}
                    block
                    ghost
                    onClick={() => paginator.next()}
                  >
                    Load More
                  </Button>
                </motion.div>
              )}
            </motion.div>
          </Container>
        </List>
      </Page>
      <AnimatePresence>
        {toggle && (
          <NewPostDirectory close={() => history.goBack()} newPost={newPost} />
        )}
      </AnimatePresence>
    </>
  );
};

export default Bulletin;
