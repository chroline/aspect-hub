import React, { useEffect, useState } from "react";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { useGroupData } from "store/GroupData";
import Page from "./page";
import Container from "components/atoms/Container";
import { useLockBodyScroll, useToggle } from "react-use";
import Header from "components/molecules/Header";
import FAB from "components/atoms/FAB";
import { useHistory, useLocation } from "react-router";
import AddIcon from "@atlaskit/icon/glyph/add";
import CapsuleView from "components/molecules/Capsule";
import { ModalTransition } from "@atlaskit/modal-dialog";
import withCSS from "structure/withCSS";
import NewCapsule from "components/organisms/NewCapsule";
import useSubscription from "util/hooks/useSubscription";
import Firebase from "store/Firebase";
import { collection } from "rxfire/firestore";
import { switchMap } from "rxjs/operators";
import { Observable, of } from "rxjs";
import Capsule from "util/interfaces/Capsule";
import { AnimatePresence, motion } from "framer-motion";

const List = withTheme(
  ({ spacings }) => css`
    display: flex;
    justify-content: center;
    padding: ${spacings[12]} ${spacings[6]} 56px ${spacings[6]};
    width: 100%;
  `
)("div");

const animationVariants = {
  container: {
    hidden: {
      opacity: 0,
    },
    show: {
      opacity: 1,
      transition: {
        staggerChildren: 0.2,
      },
    },
  },
  item: {
    hidden: { opacity: 0 },
    show: { opacity: 1 },
  },
};

const Capsules = withCSS<{ scroll: number }>(({ scroll, theme }) => {
  const [groupData] = useGroupData();
  const [showModal, setShowModal] = useToggle(false);
  const [capsules, setCapsules] = useState<Capsule[]>([]);
  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    if (location.hash === "#new") {
      if (!location.state) {
        history.replace("/capsules");
        history.push("/capsules#new", true);
        setShowModal(true);
      } else setShowModal(true);
    } else setShowModal(false);
  }, [location.hash]);

  useSubscription(() => {
    const db = Firebase.app.firestore();
    const capsules$ = collection(
      db.collection(`groups/${groupData.id}/capsules`)
    ) as Observable<firebase.firestore.QueryDocumentSnapshot<Capsule>[]>;
    return capsules$
      .pipe(
        switchMap((val) =>
          of(
            val.map((value) => ({
              id: value.id,
              ...value.data(),
            }))
          )
        )
      )
      .subscribe((value) => setCapsules(value));
  });

  useLockBodyScroll(showModal);

  return (
    <>
      <Page>
        <Header>
          <Container
            css={css`
              position: relative;
            `}
          >
            <h1>Memories</h1>
            <h2>{groupData.name}</h2>
            <FAB.Extended
              bottom={false}
              fixed={true}
              onClick={() => history.push("/capsules#new", true)}
            >
              <AddIcon label={"new post"} />
              <p>Capsule</p>
            </FAB.Extended>
          </Container>
        </Header>
        <List>
          <Container
            size={"md"}
            css={css`
              > div {
                display: grid;
                grid-template-columns: 1fr 1fr;
                @media screen and (max-width: 830px) {
                  grid-template-columns: 1fr;
                }
              }
            `}
          >
            <AnimatePresence>
              {capsules.length > 0 && (
                <motion.div
                  variants={animationVariants.container}
                  initial={"hidden"}
                  animate={"show"}
                >
                  {capsules.map(({ name, id }) => (
                    <motion.div key={id} variants={animationVariants.item}>
                      <CapsuleView name={name} id={id} />
                    </motion.div>
                  ))}
                </motion.div>
              )}
            </AnimatePresence>
          </Container>
        </List>
      </Page>
      <ModalTransition>
        {showModal && <NewCapsule close={() => history.goBack()} />}
      </ModalTransition>
    </>
  );
});

export default Capsules;
