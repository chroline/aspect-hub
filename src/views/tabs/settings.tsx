import React, { useState } from "react";
import { css } from "@emotion/core";
import { useGroupData } from "store/GroupData";
import Page from "./page";
import Container from "components/atoms/Container";
import Header from "components/molecules/Header";
import withCSS from "structure/withCSS";
import { Box, Button, Input } from "components/atoms";
import withTheme from "structure/withTheme";
import { useToggle } from "react-use";
import Groups from "ctrl/Groups";
import User from "util/interfaces/User";
import useSubscription from "util/hooks/useSubscription";
import Firebase from "store/Firebase";
import { doc } from "rxfire/firestore";
import { Observable } from "rxjs";
import Group from "util/interfaces/Group";
import { map, take } from "rxjs/operators";
import { UserX } from "react-feather";
import isError from "util/lambda/isError";
import Notifications from "store/Notifications";

const Wrapper: React.FC = withCSS(({ children, theme }) => (
  <Container
    css={css`
      padding: 0 ${theme.spacings["5"]};
      overflow: visible;
      overflow-y: scroll;
      position: relative;
    `}
    size={"sm"}
  >
    {children}
  </Container>
));

const Item = withTheme(
  ({ spacings }) => css`
    margin-bottom: ${spacings["5"]};
    margin-top: ${spacings["12"]};
  `
)("div");

const Label = withTheme(
  ({ fontSizes, fontWeights, spacings }) => css`
    color: rgba(0, 0, 0, 0.8);
    font-size: ${fontSizes.lg};
    font-weight: ${fontWeights.bold};
    margin-bottom: ${spacings["5"]};
  `
)("p");

const List = withTheme(
  ({ spacings, radius }) => css`
    background: #fafbfc;
    border: 2px solid #dfdfdf;
    border-radius: ${radius};
    margin-top: ${spacings["5"]};
    width: 100%;
  `
)("div");

const Member = withCSS<{ user: User }>((props) => {
  const { theme, user } = props;
  const [groupData] = useGroupData();
  const [loading, setLoading] = useToggle(false);

  const remove = () => {
    setLoading(true);
    Groups.removeMember(user.id, groupData.id).subscribe(() =>
      setLoading(false)
    );
  };

  return (
    <div key={user.id}>
      <span>{user.name}</span>
      <Button
        size={"small"}
        color={"error"}
        onClick={remove}
        disabled={loading}
      >
        <UserX />
      </Button>
      <style jsx>{`
        div {
          align-items: center;
          &:not(:last-of-type) {
            border-bottom: 2px solid #dfdfdf;
          }
          display: flex;
          font-size: ${theme.fontSizes.xl};
          font-weight: ${theme.fontWeights.semibold};
          padding: ${theme.spacings["5"]};
          & span {
            flex: 1;
          }
        }
      `}</style>
    </div>
  );
});

const Settings = withCSS<{ scroll: number }>(({ scroll, theme }) => {
  const [groupData] = useGroupData();
  const [loadingName, setLoadingName] = useToggle(false);
  const [name, setName] = useState(groupData.name);
  const [users, setUsers] = useState<User[]>([]);
  const [loadingEmail, setLoadingEmail] = useToggle(false);
  const [memberEmail, setMemberEmail] = useState("");

  const changeName = () => {
    setLoadingName(true);
    Groups.changeName(name, groupData.id).subscribe(() =>
      setLoadingName(false)
    );
  };

  const addMember = () => {
    setLoadingEmail(true);
    Groups.addMember(memberEmail, groupData.id).subscribe((res) => {
      let err = res as Error;
      if (isError(err)) {
        Notifications.push({
          type: "error",
          title: "Couldn't find user with email ",
          desc: memberEmail,
        });
      }
      setMemberEmail("");
      setLoadingEmail(false);
    });
  };

  useSubscription(() => {
    const db = Firebase.app.firestore();
    return (doc(db.doc(`groups/${groupData.id}`)) as Observable<
      firebase.firestore.DocumentSnapshot<Group>
    >)
      .pipe(map((val) => val.data()?.members))
      .subscribe(async (value) => {
        Promise.all(
          (value || []).map(async (user) => {
            const u = await (doc(db.doc(`users/${user}`)) as Observable<
              firebase.firestore.DocumentSnapshot<User>
            >)
              .pipe(take(1))
              .toPromise();
            return { id: u.id, ...u.data() };
          })
        ).then((v) => setUsers(v as any));
      });
  });

  return (
    <>
      <Page>
        <Header>
          <Container
            css={css`
              position: relative;
            `}
          >
            <h1>Settings</h1>
            <h2>{groupData.name}</h2>
          </Container>
        </Header>
        <Box
          css={css`
            display: flex;
            justify-content: center;
          `}
        >
          <Wrapper>
            <Item>
              <Label>Group name</Label>
              <Input
                color={"primary"}
                value={name}
                disabled={loadingName}
                onChange={(e) => setName(e.target.value)}
                onKeyUp={(e) => e.keyCode === 13 && changeName()}
              />
            </Item>
            <Item>
              <Label>Members</Label>
              <Input
                placeholder={"New member email..."}
                color={"primary"}
                value={memberEmail}
                disabled={loadingEmail}
                onChange={(e) => setMemberEmail(e.target.value)}
                onKeyUp={(e) => e.keyCode === 13 && addMember()}
              />
              <List>
                {users.map((u) => (
                  <Member user={u} />
                ))}
              </List>
            </Item>
          </Wrapper>
        </Box>
      </Page>
    </>
  );
});

export default Settings;
