import React, { useState } from "react";
import RegisterForm from "components/molecules/AuthForm/register";
import { AuthPage } from "views/auth/pageElements";
import { useAuthenticationFlow } from "ctrl/AuthenticationFlow";
import { of } from "rxjs";
import { catchError } from "rxjs/operators";
import AuthProvider from "ctrl/Auth";
import { FirebaseError } from "firebase/app";
import isError from "util/lambda/isError";

const Register = () => {
  const [{ context }, send] = useAuthenticationFlow();
  const [ctrl, setCtrl] = useState({
    disabled: false,
    error: "",
  });

  const onSubmit = () => {
    setCtrl({
      disabled: true,
      error: "",
    });
    AuthProvider.fetchSignInMethodsForEmail(context.email)
      .pipe(
        catchError((err: Error) => {
          return of(err);
        })
      )
      .subscribe((res) => {
        let methods = res as string[];
        let err = res as FirebaseError;
        if (isError(err)) {
          setCtrl({
            disabled: false,
            error: "an error occurred, please try again",
          });
        } else if (methods.length === 0) send("REGISTER");
        else
          setCtrl({
            disabled: false,
            error: "this email is already in use",
          });
      });
  };

  return (
    <AuthPage key={"register"} error={ctrl.error}>
      <RegisterForm key={"form"} onSubmit={onSubmit} disabled={ctrl.disabled} />
    </AuthPage>
  );
};

export default Register;
