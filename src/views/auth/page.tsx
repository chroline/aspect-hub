import React from "react";
import { motion } from "framer-motion";
import { colors } from "util/theme/colors";
import styled from "@emotion/styled";
import get from "util/lambda/get";
import { transition } from "util/theme/constants";

const pageVariants = {
  initial: {
    opacity: 0,
    y: 30,
    scale: 0.95,
  },
  in: {
    opacity: 1,
    y: 0,
    scale: 1,
  },
  out: {
    opacity: 0,
    y: -20,
    scale: 1.025,
  },
};

const MotionPage = styled(motion.div)`
  flex: 1;
  width: 100%;
`;

const Page: React.FC = ({ children }) => (
  <>
    <MotionPage
      initial="initial"
      animate="in"
      exit="out"
      variants={pageVariants}
      transition={transition}
    >
      {children}
    </MotionPage>
    <style jsx>{`
      :global(body) {
        background: ${get("muted", colors)};
      }
    `}</style>
  </>
);

export default Page;
