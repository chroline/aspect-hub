import React, { useState } from "react";
import AuthProvider from "ctrl/Auth";
import { catchError } from "rxjs/operators";
import { of } from "rxjs";
import LoginForm from "components/molecules/AuthForm/login";
import { AuthPage } from "views/auth/pageElements";
import { FirebaseError } from "firebase/app";
import { useAuthenticationFlow } from "ctrl/AuthenticationFlow";
import isError from "util/lambda/isError";

const Login = () => {
  const [{ context }, send] = useAuthenticationFlow();
  const [ctrl, setCtrl] = useState({
    disabled: false,
    error: "",
  });

  const onSubmit = () => {
    setCtrl({
      disabled: true,
      error: "",
    });
    AuthProvider.emailSignIn(context.email, context.pwd)
      .pipe(
        catchError((err) => {
          return of(err as Error);
        })
      )
      .subscribe((res) => {
        let err = res as FirebaseError;
        if (isError(err)) {
          let error: string;
          switch (err.code) {
            case "auth/wrong-password":
              error = "incorrect password";
              break;
            default:
              error = "an error occurred, please try again";
              console.log(err.code);
              break;
          }
          setCtrl({
            disabled: false,
            error,
          });
        } else send("LOADING");
      });
  };

  return (
    <AuthPage key={"login"} error={ctrl.error}>
      <LoginForm onSubmit={onSubmit} disabled={ctrl.disabled} />
    </AuthPage>
  );
};

export default Login;
