import { AnimatePresence, motion } from "framer-motion";
import Alert from "components/atoms/Alert";
import useWindowSize from "util/hooks/useWindowSize";
import { transition } from "util/theme/constants";
import { Logo as LogoSVG } from "components/atoms";
import Page from "views/auth/page";
import React from "react";
import { css } from "@emotion/core";
import withTheme from "structure/withTheme";
import remToPx from "util/lambda/remToPx";

export const Wrapper = withTheme(
  () => css`
    align-items: center;
    display: flex;
    height: 100%;
    flex: 1;
    justify-content: center;
    overflow: hidden;
    width: 100%;
  `
)("div");

export const Content = withTheme(
  () => css`
    align-items: center;
    display: flex;
    flex-direction: row;
    justify-content: center;
    max-width: 50rem;
    width: 100%;
    @media only screen and (max-width: 55rem) {
      flex-direction: column;
      max-width: 30rem;
    }
  `
)("div");

export const LogoBox = withTheme(
  () => css`
    align-items: center;
    display: flex;
    max-width: 23rem;
    width: 100%;
  `
)(motion.div);

export const ErrorBox = withTheme(
  ({ spacings }) => css`
    margin-top: ${spacings[6]};
    width: 100%;
  `
)(Alert);

export const FormBox = withTheme(
  ({ spacings }) => css`
    align-items: center;
    display: flex;
    flex-direction: column;
    flex: 1;
    padding: 0 ${spacings[8]};
  `
)("div");

export const InputContainer = withTheme(
  ({ spacings }) => css`
    margin: ${spacings[6]} 0;
  `
)("div");

export const AuthPage: React.FC<{ error: string | null }> = ({
  children,
  error,
}) => (
  <Page>
    <Wrapper>
      <Content>
        {useWindowSize().width > remToPx(55) && (
          <LogoBox
            initial={{ opacity: 0 }}
            animate={{ opacity: 1, transition: transition }}
            exit={{ opacity: 1, transition: { duration: 0 } }}
          >
            <LogoSVG />
          </LogoBox>
        )}
        <FormBox>
          <AnimatePresence>
            {useWindowSize().width < remToPx(55) && (
              <LogoBox
                key={"small-logo"}
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: transition }}
                exit={{ opacity: 0, transition: { duration: 0 } }}
                positionTransition={transition}
              >
                <LogoSVG />
              </LogoBox>
            )}
            {error && (
              <motion.div
                initial={{ y: 20, opacity: 0 }}
                animate={{
                  y: 0,
                  opacity: 1,
                }}
                exit={{ y: 20, opacity: 0 }}
                transition={transition}
                key={"error"}
                positionTransition={transition}
                style={{ width: "100%" }}
              >
                <ErrorBox type={"error"}>
                  <p>
                    <b>Error: </b>
                    {error}
                  </p>
                </ErrorBox>
              </motion.div>
            )}
            {children}
          </AnimatePresence>
        </FormBox>
      </Content>
    </Wrapper>
  </Page>
);
