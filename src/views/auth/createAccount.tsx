import { Input } from "components/atoms";
import React, { ChangeEventHandler, useState } from "react";
import { AuthPage, InputContainer } from "views/auth/pageElements";
import AuthForm from "components/molecules/AuthForm";
import { catchError } from "rxjs/operators";
import { of } from "rxjs";
import { useAuthenticationFlow } from "ctrl/AuthenticationFlow";
import iife from "util/iife";
import AuthProvider from "ctrl/Auth";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { FirebaseError } from "firebase/app";
import isError from "util/lambda/isError";
import Users from "ctrl/Users";

const Disclaimer = withTheme(
  ({ fontSizes }) => css`
    color: black;
    font-size: ${fontSizes.lg};
    line-height: 1.4;
    margin: 0;
    text-align: center;
  `
)("p");

const Message = withTheme(
  ({ spacings, colors, fontSizes }) => css`
    color: ${colors.error};
    margin: ${spacings[2]};
    font-size: ${fontSizes.sm};
  `
)("p");

interface Errors {
  pwd: string | null;
}

const validation = (fields: { pwd: string }): Errors => {
  const pwd = iife(() => {
    if (!fields.pwd || fields.pwd === "" || fields.pwd === " ")
      return "Password is required";
    if (fields.pwd.length < 6) return "Must be at least 6 characters";
    return null;
  });
  return { pwd };
};

const CreateAccount = () => {
  const [errors, setErrors] = useState<Partial<Errors>>({});

  const [{ context }, send] = useAuthenticationFlow();
  const [ctrl, setCtrl] = useState({
    disabled: false,
    error: "",
  });

  const handleSubmit = () => {
    const errors = validation(context);
    if (errors.pwd !== null)
      return requestAnimationFrame(() => setErrors(errors));
    setCtrl({
      disabled: true,
      error: "",
    });
    AuthProvider.emailSignUp(context.email, context.pwd)
      .pipe(catchError((err) => of(err as Error)))
      .subscribe((res) => {
        let err = res as FirebaseError;
        if (isError(err))
          setCtrl({
            disabled: false,
            error: "an error occurred, please try again",
          });
        else {
          send("LOADING");
          Users.create(
            (AuthProvider.user() as firebase.User).uid,
            context.email,
            context.name
          )
            .pipe(catchError((err) => of(err as Error)))
            .subscribe((res) => {
              let err = res as FirebaseError;
              if (isError(err))
                setCtrl({
                  disabled: false,
                  error: "an error occurred, please try again",
                });
            });
        }
      });
  };

  const pwdChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setErrors((err) => ({ ...err, pwd: null }));
    send({ type: "INPUT_PWD", value: e.target.value });
  };

  return (
    <AuthPage key={"pwd"} error={ctrl.error}>
      <AuthForm
        key={"form"}
        disabled={ctrl.disabled}
        handleSubmit={handleSubmit}
      >
        <div style={{ marginTop: "1.5rem" }}>
          <Disclaimer>Please create a password below.</Disclaimer>
        </div>
        <InputContainer>
          <Input
            color={"primary"}
            name={"password"}
            placeholder={"Password"}
            type={"password"}
            onChange={pwdChange}
            value={context.pwd}
          />
        </InputContainer>
        <Message>{errors.pwd}</Message>
      </AuthForm>
    </AuthPage>
  );
};

export default CreateAccount;
