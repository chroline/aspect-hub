import React from "react";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import Color from "color";
import { motion } from "framer-motion";
import { transition } from "util/theme/constants";
import { useHistory } from "react-router";

const Inner = withTheme(
  ({ colors, spacings, fontSizes, fontWeights, fonts }) => css`
    color: ${Color(colors.muted).darken(0.5).toString()};
    display: inline-block;
    font-size: ${fontSizes["xl"]};
    line-height: 1.3;
    text-align: center;
    max-width: 20rem;
    width: 100%;
    & p:first-of-type {
      font-family: ${fonts.serif};
      font-size: ${fontSizes["3xl"]};
      font-weight: ${fontWeights.bold};
      margin-bottom: ${spacings["3"]};
    }
    & button {
      background: none;
      border: none;
      color: ${colors.primary};
      font-weight: ${fontWeights.bold};
      text-decoration: none;
      padding: 0;
    }
  `
)("div");

const NoGroup = () => {
  const history = useHistory();
  return (
    <>
      <div>
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={transition}
        >
          <Inner>
            <p>Nothing to see here!</p>
            <p>
              Please choose a group by visiting the{" "}
              <button onClick={() => history.push("/menu", true)}>
                Groups
              </button>{" "}
              page.
            </p>
          </Inner>
        </motion.div>
      </div>
      <style jsx>{`
        div {
          align-items: center;
          display: flex;
          height: 100%;
          flex-direction: column;
          justify-content: center;
          width: 100%;
        }
      `}</style>
    </>
  );
};

export default NoGroup;
