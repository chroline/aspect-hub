import React, { useEffect, useRef, useState } from "react";
import { BackBtn, Page } from "./pageElements";
import Header from "components/molecules/Header";
import withCSS from "structure/withCSS";
import { css } from "@emotion/core";
import NavBar from "components/molecules/NavBar";
import { default as CloseIcon } from "@atlaskit/icon/glyph/arrow-left";
import Container from "components/atoms/Container";
import { transition } from "util/theme/constants";
import { useHistory, useLocation, useRouteMatch } from "react-router";
import useSubscription from "util/hooks/useSubscription";
import Firebase from "store/Firebase";
import { doc } from "rxfire/firestore";
import { useGroupData } from "store/GroupData";
import Notifications from "store/Notifications";
import { Observable, of } from "rxjs";
import Capsule from "util/interfaces/Capsule";
import FAB from "components/atoms/FAB";
import { MoreVertical } from "react-feather";
import { Button } from "components/atoms";
import PostsRenderer from "components/organisms/PostsRenderer";
import { CapsulePostsPaginator } from "store/Paginators";
import Post from "util/interfaces/Post";
import { filter, switchMap } from "rxjs/operators";
import withTheme from "structure/withTheme";
import { useScroll, useToggle } from "react-use";
import EditCapsule from "components/organisms/EditCapsule";
import { ModalTransition } from "@atlaskit/modal-dialog";
import CapsuleLocked from "components/organisms/CapsuleLocked";

const List = withTheme(
  ({ spacings }) => css`
    display: flex;
    flex: 1;
    justify-content: center;
    padding: 0 ${spacings[6]} 0 ${spacings[6]};
    width: 100%;
  `
)("div");

const CapsuleView = withCSS(({ theme }) => {
  const history = useHistory();
  const location = useLocation();
  const [name, setName] = useState("");
  const [lock, setLock] = useState<Date>();
  const [groupData] = useGroupData();
  const [showModal, setShowModal] = useToggle(false);
  const [active, setActive] = useToggle(true);
  const [posts, setPosts] = useState<Post[]>([]);
  const [more, setMore] = useState(false);
  const scrollRef = useRef<HTMLElement>(null);
  const { y: scroll } = useScroll(scrollRef);
  let { current: paginator } = useRef<CapsulePostsPaginator<Post>>();

  useEffect(() => {
    if (location.hash === "#edit") {
      if (!location.state) {
        history.replace(location.pathname);
        history.push(location.pathname + "#edit", true);
        setShowModal(true);
      } else setShowModal(true);
    } else setShowModal(false);
  }, [location.hash]);

  const match = useRouteMatch<{
    page: string;
  }>("/capsule/:page");
  useSubscription(() => {
    paginator =
      paginator ||
      new CapsulePostsPaginator<Post>(
        Firebase.app.firestore(),
        `groups/${groupData?.id as string}/capsules/${
          match?.params.page || "noop"
        }`,
        25
      );
    return paginator.subject
      .pipe(
        switchMap((val) =>
          of(
            val.map((value) => ({
              id: value.id,
              ...value.data(),
            }))
          )
        )
      )
      .subscribe((value) => {
        if (value.length < (paginator?.amnt || 0)) setMore(true);
        else setMore(false);
        setPosts(value);
      });
  });

  useEffect(() => {
    const sub = (doc(
      Firebase.app
        .firestore()
        .doc(
          `groups/${groupData?.id as string}/capsules/${
            match?.params.page || ""
          }`
        )
    ) as Observable<firebase.firestore.DocumentSnapshot<Capsule>>)
      .pipe(filter(() => active))
      .subscribe((value) => {
        console.log(active);
        if (!value.exists) {
          Notifications.push({
            type: "error",
            title: "Error retrieving capsule: ",
            desc: "we couldn't find this capsule.",
          });
          history.replace("/capsules");
        } else {
          setName(value.data()?.name || "");
          setLock(value.data()?.lock?.toDate());
        }
      });
    return () => sub.unsubscribe();
  }, [active]);

  return (
    <>
      <Page.Wrapper
        initial={"initial"}
        animate={"in"}
        exit={"out"}
        variants={Page.animation}
        transition={transition}
        ref={scrollRef}
      >
        <NavBar title={"Capsule"} scroll={scroll}>
          <BackBtn
            onClick={() =>
              location.state ? history.goBack() : history.replace("/capsules")
            }
          >
            <CloseIcon label={"close"} />
          </BackBtn>
        </NavBar>

        <Header
          css={css`
            border-bottom: 1px solid #dbdbdb;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.02),
              0 2px 1px rgba(0, 0, 0, 0.012), 0 1px 1px rgba(0, 0, 0, 0.014);
          `}
        >
          <Container
            css={css`
              position: relative;
            `}
          >
            <h1>{name}</h1>
            <FAB.Normal
              scroll={0}
              onClick={() => history.push(location.pathname + "#edit", true)}
              muted
            >
              <MoreVertical />
            </FAB.Normal>
          </Container>
        </Header>
        <List>
          <Container
            size={"sm"}
            css={css`
              height: 100%;
              padding-bottom: ${theme.spacings[12]};
            `}
          >
            {!lock ||
            new Date().toJSON().slice(0, 10) >= lock?.toJSON().slice(0, 10) ? (
              <div
                style={{
                  paddingTop: theme.spacings[12],
                }}
              >
                <PostsRenderer posts={posts} />
                {more && (
                  <Button
                    color={"primary"}
                    block
                    ghost
                    onClick={() => paginator?.next()}
                  >
                    Load More
                  </Button>
                )}
              </div>
            ) : (
              <CapsuleLocked date={lock} />
            )}
          </Container>
        </List>
      </Page.Wrapper>
      <ModalTransition>
        {showModal && (
          <EditCapsule
            close={() => history.goBack()}
            exit={() =>
              new Observable((s) => {
                setActive(false);
                history.goBack();
                location.state
                  ? history.goBack()
                  : history.replace("/capsules");
                s.next();
              })
            }
            id={match?.params.page || "noop"}
          />
        )}
      </ModalTransition>
    </>
  );
});

export default CapsuleView;
