import React from "react";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { motion } from "framer-motion";
import { transition } from "util/theme/constants";
import {
  AlertCircle as AlertCircleIcon,
  Film as FilmIcon,
  Type as TypeIcon,
} from "react-feather";
import OutsideClickHandler from "react-outside-click-handler";

class Page {
  static Wrapper = withTheme(
    () => css`
      align-items: center;
      background: white;
      box-shadow: 0 10px 20px rgba(0, 0, 0, 0.3);
      display: flex;
      flex-direction: column;
      height: 100%;
      justify-content: center;
      left: 0;
      position: fixed;
      top: 0;
      width: 100%;
      z-index: 9;
      @supports (
        (-webkit-backdrop-filter: saturate(100%) blur(50px)) or
          (backdrop-filter: saturate(100%) blur(50px))
      ) {
        backdrop-filter: saturate(100%) blur(50px);
        background: rgba(255, 255, 255, 0.75);
      }
    `
  )(motion.div);

  static animation = {
    initial: {
      opacity: 0,
      scale: 1.1,
      y: 0,
    },
    in: {
      opacity: 1,
      scale: 1,
      y: 0,
    },
    out: {
      opacity: 0,
      scale: 1,
      y: 100,
    },
  };
}

class Options {
  static Wrapper = withTheme(
    ({ spacings }) => css`
      border-radius: 10px;
      padding: ${spacings["5"]};
      max-width: 100%;
    `
  )("div");

  static Item = withTheme(
    ({ colors, spacings, fontSizes }) => css`
      align-items: center;
      background: transparent;
      border: none;
      color: ${colors.primary};
      display: flex;
      margin: 0 ${spacings["4"]};
      padding: ${spacings["6"]} 0;
      text-align: left;
      width: 100%;
      svg {
        height: 3rem;
        margin-right: ${spacings["5"]};
        width: 3rem;
      }
      p {
        color: black;
        font-size: ${fontSizes["2xl"]};
        width: 100%;
      }
    `
  )("button");
}

const NewPostDirectory: React.FC<{
  close: () => void;
  newPost: (type: string) => () => void;
}> = ({ close, newPost }) => {
  return (
    <Page.Wrapper
      initial={"initial"}
      animate={"in"}
      exit={"out"}
      variants={Page.animation}
      transition={transition}
    >
      <OutsideClickHandler onOutsideClick={close}>
        <Options.Wrapper>
          <Options.Item onClick={newPost("text")}>
            <TypeIcon />
            <p>Text</p>
          </Options.Item>
          <Options.Item onClick={newPost("announcement")}>
            <AlertCircleIcon />
            <p>Announcement</p>
          </Options.Item>
          <Options.Item onClick={newPost("media")}>
            <FilmIcon />
            <p>Media</p>
          </Options.Item>
        </Options.Wrapper>
      </OutsideClickHandler>
    </Page.Wrapper>
  );
};

export default NewPostDirectory;
