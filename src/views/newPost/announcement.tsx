import React, { useRef, useState } from "react";
import { BackBtn, Page } from "./pageElements";
import Header from "components/molecules/Header";
import withCSS from "structure/withCSS";
import { css } from "@emotion/core";
import NavBar from "components/molecules/NavBar";
import { default as CloseIcon } from "@atlaskit/icon/glyph/arrow-left";
import SendIcon from "@atlaskit/icon/glyph/send";
import { Box, Button, TextArea } from "components/atoms";
import Container from "components/atoms/Container";
import { transition } from "util/theme/constants";
import { useHistory, useLocation } from "react-router";
import { useGroupData } from "store/GroupData";
import Posts from "ctrl/Posts";
import { FirebaseError } from "firebase/app";
import isError from "util/lambda/isError";

const NewAnnouncementPost = withCSS(({ theme }) => {
  const history = useHistory();
  const location = useLocation();
  const contentRef = useRef<HTMLTextAreaElement>();
  const [disabled, setDisabled] = useState(false);

  const [groupID] = useGroupData();

  const create = () => {
    requestAnimationFrame(() => setDisabled(true));
    Posts.create(groupID.id, "announcement", {
      content: contentRef.current?.value || "",
    }).subscribe((val) => {
      let err = val as FirebaseError;
      !isError(err) && history.replace("/");
    });
  };

  return (
    <Page.Wrapper
      initial={"initial"}
      animate={"in"}
      exit={"out"}
      variants={Page.animation}
      transition={transition}
    >
      <NavBar title={""} scroll={0}>
        <BackBtn
          onClick={() =>
            location.state ? history.goBack() : history.replace("/bulletin")
          }
        >
          <CloseIcon label={"close"} />
        </BackBtn>
      </NavBar>
      <Header
        css={css`
          h1 {
            font-family: ${theme.fonts["sans-serif"]};
            font-weight: ${theme.fontWeights.medium};
          }
        `}
      >
        <h1>Make an Announcement</h1>
      </Header>
      <Box
        css={css`
          display: flex;
          justify-content: center;
        `}
      >
        <Container
          css={css`
            padding: 2rem;
          `}
        >
          <TextArea
            color={"primary"}
            disabled={disabled}
            placeholder={"Write your announcement here..."}
            rows={5}
            ref={contentRef}
            maxLength={90}
          />
          <Box
            css={css`
              padding: ${theme.spacings["6"]} 0;
            `}
          >
            <Button
              css={css`
                margin-left: auto;
              `}
              onClick={create}
              color={"primary"}
              size={"xlarge"}
            >
              <Box
                css={css`
                  padding-right: 0.95rem;
                  svg {
                    display: block;
                  }
                `}
              >
                <SendIcon label={"send"} />
              </Box>
              SAVE
            </Button>
          </Box>
        </Container>
      </Box>
    </Page.Wrapper>
  );
});

export default NewAnnouncementPost;
