import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { motion } from "framer-motion";

export const BackBtn = withTheme(
  ({ colors, fontSizes, fontWeights }) => css`
    background: none;
    border: none;
    color: ${colors.primary};
    cursor: pointer;
    font-size: ${fontSizes["xl"]};
    font-weight: ${fontWeights.bold};
    padding: 0;
  `
)("button");

export class Page {
  static Wrapper = withTheme(
    ({ colors }) => css`
      background: ${colors.muted};
      box-shadow: 10px 0 20px rgba(0, 0, 0, 0.3);
      height: 100%;
      left: 0;
      overflow: scroll;
      position: fixed;
      top: 0;
      width: 100%;
      z-index: 100;
    `
  )(motion.div);

  static animation = {
    initial: {
      opacity: 0,
      x: 200,
    },
    in: {
      opacity: 1,
      x: 0,
    },
    out: {
      opacity: 0,
      x: 140,
    },
  };
}
