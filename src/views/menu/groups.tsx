import React, { useState } from "react";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { useGroupData } from "store/GroupData";
import { from, of } from "rxjs";
import AuthProvider from "ctrl/Auth";
import { AnimatePresence, motion } from "framer-motion";
import { transition } from "util/theme/constants";
import Container from "components/atoms/Container";
import withCSS from "structure/withCSS";
import useSubscription from "util/hooks/useSubscription";
import Firebase from "store/Firebase";
import Group from "util/interfaces/Group";
import { catchError, map } from "rxjs/operators";
import { useToggle } from "react-use";
import { FAB } from "components/atoms";
import AddIcon from "@atlaskit/icon/glyph/add";
import { ModalTransition } from "@atlaskit/modal-dialog";
import NewGroup from "components/organisms/NewGroup";
import { useHistory } from "react-router";
import isError from "util/lambda/isError";

const Page = withTheme(
  () => css`
    display: flex;
    height: 100%;
    justify-content: center;
    position: absolute;
    overflow: hidden;
    width: 100%;
  `
)(motion.div);

const Wrapper: React.FC = withCSS(({ children, theme }) => (
  <Container
    css={css`
      flex: 1;
      margin: 0 ${theme.spacings["5"]};
      position: relative;
      & > div {
        height: 100%;
        overflow: scroll;
      }
    `}
  >
    {children}
  </Container>
));

const Item = withTheme(
  ({ spacings, fontSizes, fontWeights }) => css`
    background: transparent;
    border: none;
    border-bottom: 1px solid #e0e0e0;
    padding: ${spacings["8"]} ${spacings["5"]};
    text-align: left;
    width: 100%;
    &:last-of-type {
      border-bottom: 1px solid #e0e0e0;
    }
    & h4 {
      font-size: ${fontSizes["2xl"]};
      font-weight: ${fontWeights.medium};
    }
  `
)(motion.button);

const Groups: React.FC<{ close: () => void }> = ({ close }) => {
  const history = useHistory();
  const [groups, setGroups] = useState<Group[]>([]);
  const [showModal, setShowModal] = useToggle(false);
  const [, setGroupData] = useGroupData();

  const loadGroup = (group: Group) => () => {
    setGroupData(group);
    close();
  };

  useSubscription(() =>
    from(
      Firebase.app
        .firestore()
        .collection(`groups`)
        .where("members", "array-contains", AuthProvider.user()?.uid || "noop")
        .get() as Promise<firebase.firestore.QuerySnapshot<Group>>
    )
      .pipe(
        catchError((err) => of(err as Error)),
        map((value) =>
          isError(value as Error)
            ? value
            : (value as firebase.firestore.QuerySnapshot<Group>).docs.map(
                (v) => ({
                  id: v.id,
                  ...v.data(),
                })
              )
        )
      )
      .subscribe((val) => !isError(val as Error) && setGroups(val as any))
  );

  return (
    <>
      <Page
        initial={{
          x: "-100%",
        }}
        animate={{
          x: 0,
        }}
        exit={{
          x: "-100%",
        }}
        transition={transition}
      >
        <Wrapper>
          <div>
            <AnimatePresence>
              {groups.map((group) => (
                <Item
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  exit={{ opacity: 0 }}
                  transition={transition}
                  key={group.id}
                  onClick={loadGroup(group)}
                >
                  <h4>{group.name}</h4>
                </Item>
              ))}
            </AnimatePresence>
          </div>
          <FAB.Extended
            onClick={() => setShowModal(true)}
            bottom={true}
            fixed={false}
          >
            <AddIcon label={"new group"} />
            <p>New</p>
          </FAB.Extended>
        </Wrapper>
      </Page>
      <ModalTransition>
        {showModal && (
          <NewGroup
            close={() => setShowModal(false)}
            exit={(group) => {
              setShowModal(false);
              setGroupData(group);
              history.goBack();
            }}
          />
        )}
      </ModalTransition>
    </>
  );
};

export default Groups;
