import React, { useState } from "react";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { AnimatePresence, motion } from "framer-motion";
import { transition } from "util/theme/constants";
import { default as CloseIcon } from "@atlaskit/icon/glyph/arrow-down";
import Container from "components/atoms/Container";
import MenuTabs from "views/menu/tabs";
import Groups from "views/menu/groups";
import Profile from "views/menu/profile";
import { Box } from "components/atoms";
import withCSS from "structure/withCSS";
import { useHistory, useLocation } from "react-router";

const TopBar = withTheme(
  ({ fontSizes, fontWeights, colors }) => css`
    align-items: center;
    background: ${colors.muted};
    display: flex;
    flex-direction: row;
    flex-shrink: 0;
    justify-content: flex-start;
    height: 56px;
    padding: 0 12px;
    width: 100%;
    button {
      background: none;
      border: none;
      color: ${colors.primary};
      font-size: ${fontSizes["xl"]};
      font-weight: ${fontWeights.bold};
      padding: 0;
    }
  `
)("div");

const MenuWrapper = withTheme(
  () => css`
    align-items: center;
    background: white;
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.3);
    display: flex;
    flex-direction: column;
    height: 100%;
    left: 0;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 9;
  `
)(motion.div);

const Header = withTheme(
  ({ colors, fonts, fontWeights, fontSizes, spacings }) => css`
    background: ${colors.muted};
    font-family: ${fonts["sans-serif"]};
    font-weight: ${fontWeights.bold};
    font-size: ${fontSizes["5xl"]};
    padding-bottom: ${spacings["8"]};
    padding-top: calc(${spacings["24"]} - 56px);
    text-align: center;
    width: 100%;
  `
)("div");

const MenuPage = withCSS(({ theme }) => {
  const location = useLocation();
  const history = useHistory();
  const close = () =>
    location.state ? history.goBack() : history.replace("/bulletin");

  const [tab, setTab] = useState(0);

  return (
    <MenuWrapper
      initial={{ y: 200, opacity: 0 }}
      animate={{ y: 0, opacity: 1 }}
      exit={{ y: 100, opacity: 0 }}
      transition={transition}
    >
      <TopBar>
        <button onClick={close}>
          <CloseIcon label={"close"} />
        </button>
      </TopBar>
      <Header>Menu</Header>
      <Box
        css={css`
          background: ${theme.colors.muted};
          width: 100%;
        `}
      >
        <Container
          css={css`
            margin: auto;
          `}
        >
          <MenuTabs value={tab} onChange={(_, newValue) => setTab(newValue)} />
        </Container>
      </Box>
      <Box
        css={css`
          display: flex;
          height: 100%;
          justify-content: center;
          position: relative;
          width: 100%;
        `}
      >
        <AnimatePresence initial={false}>
          {tab === 0 && <Groups close={close} key={"groups"} />}
          {tab === 1 && <Profile close={close} key={"profile"} />}
        </AnimatePresence>
      </Box>
    </MenuWrapper>
  );
});

export default MenuPage;
