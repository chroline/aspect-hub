import React, { useState } from "react";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { motion } from "framer-motion";
import { transition } from "util/theme/constants";
import Container from "components/atoms/Container";
import withCSS from "structure/withCSS";
import { Box, Button, Input } from "components/atoms";
import AvatarChooser from "components/molecules/AvatarChooser";
import useSubscription from "util/hooks/useSubscription";
import AuthProvider from "ctrl/Auth";
import { useGroupData, useProfileCache } from "store/GroupData";
import { Save as SaveIcon } from "react-feather";
import { useToggle } from "react-use";
import Users from "ctrl/Users";
import { put } from "rxfire/storage";
import Firebase from "store/Firebase";
import { useMediaCache } from "store/MediaCache";
import { take } from "rxjs/operators";

const Page = withTheme(
  () => css`
    display: flex;
    height: 100%;
    justify-content: center;
    position: absolute;
    overflow: hidden;
    width: 100%;
  `
)(motion.div);

const Wrapper: React.FC = withCSS(({ children, theme }) => (
  <Container
    css={css`
      padding: 0 ${theme.spacings["5"]};
      overflow: visible;
      overflow-y: scroll;
      position: relative;
    `}
    size={"sm"}
  >
    {children}
  </Container>
));

const Item = withTheme(
  ({ spacings }) => css`
    margin-bottom: ${spacings["5"]};
    margin-top: ${spacings["12"]};
  `
)("div");

const Label = withTheme(
  ({ fontSizes, fontWeights, spacings }) => css`
    color: rgba(0, 0, 0, 0.8);
    font-size: ${fontSizes.lg};
    font-weight: ${fontWeights.bold};
    margin-bottom: ${spacings["5"]};
  `
)("p");

const Line = withTheme(
  ({ spacings, radius }) =>
    css`
      border: 1px solid #dfdfdf;
      border-radius: ${radius};
      margin-top: ${spacings["12"]};
    `
)("hr");

const Profile = withCSS<{ close: () => void }>(({ close, theme }) => {
  const [, { get }] = useProfileCache();
  const [file, setFile] = useState<File | Blob>();
  const [loading, setLoading] = useToggle(false);
  const [, { set }] = useMediaCache();
  const [, setGroupData] = useGroupData();
  const [name, setName] = useState("");
  const [pfp, setPfp] = useState<string | null>(null);

  useSubscription(() =>
    get(AuthProvider.user()?.uid || "")
      .pipe(take(1))
      .subscribe((user) =>
        setPfp(user.data()?.pfp ? `pfp/${user.data()?.pfp}` : `pfp/default.png`)
      )
  );

  useSubscription(() =>
    get(AuthProvider.user()?.uid || "").subscribe((user) =>
      setName(user.data()?.name || "")
    )
  );

  const update = async () => {
    setLoading(true);
    const uid = AuthProvider.user()?.uid;
    const storage = Firebase.app.storage();
    let pfp;
    if (file) {
      pfp = `${uid}.${file?.type.split("/")[1]}`;
      try {
        await put(storage.ref(`pfp/${pfp}`), file).toPromise();
        set(`pfp/${pfp}`);
      } catch (err) {
        console.log(err);
      }
    }
    Users.update(AuthProvider.user()?.uid || "", {
      name,
      ...(pfp ? { pfp } : { pfp: null }),
    }).subscribe(() => setLoading(false));
  };

  const signOut = () =>
    AuthProvider.signOut().subscribe(() => setGroupData(undefined));

  return (
    <Page
      initial={{
        x: "100%",
      }}
      animate={{
        x: 0,
      }}
      exit={{
        x: "100%",
      }}
      transition={transition}
    >
      <Wrapper>
        <Item>
          <Label>Profile name</Label>
          <Input
            color={"primary"}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </Item>
        {pfp && (
          <Item>
            <Label>Avatar</Label>
            <Box
              css={css`
                display: flex;
                padding: 0 ${theme.spacings["5"]};
                input {
                  visibility: hidden;
                }
              `}
            >
              <AvatarChooser
                onChange={(file) => {
                  setFile(file);
                }}
                key={"avatar-chooser"}
              />
            </Box>
          </Item>
        )}
        <Box>
          <Button
            css={css`
              margin-left: auto;
            `}
            color={"primary"}
            size={"xlarge"}
            disabled={loading}
            onClick={update}
          >
            <Box
              css={css`
                padding-right: 0.95rem;
                svg {
                  display: block;
                }
              `}
            >
              <SaveIcon />
            </Box>
            SAVE
          </Button>
        </Box>
        <Line />
        <Item>
          <Button color={"error"} ghost block onClick={signOut}>
            Sign out
          </Button>
        </Item>
      </Wrapper>
    </Page>
  );
});

export default Profile;
