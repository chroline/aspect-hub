import React from "react";
import { makeStyles, withStyles } from "@bit/mui-org.material-ui.styles";
import MuiTabs from "@bit/mui-org.material-ui.tabs";
import MuiTab from "@bit/mui-org.material-ui.tab";
import theme from "util/theme";

const Tabs = withStyles({
  root: {
    background: theme.colors.muted,
    borderBottom: "1px solid #e0e0e0",
  },
  indicator: {
    backgroundColor: theme.colors.primary,
  },
})(MuiTabs);

const Tab = withStyles(() => ({
  root: {
    textTransform: "none",
    minWidth: 72,
    maxWidth: "100%",
    flex: 1,
    fontWeight: theme.fontWeights.medium as any,
    fontFamily: theme.fonts["sans-serif"],
    fontSize: theme.fontSizes.xl,
    "&:hover": {
      color: theme.colors.primary,
      opacity: 1,
    },
    "&$selected": {
      color: theme.colors.primary,
    },
    "&:focus": {
      color: theme.colors.primary,
    },
  },
  selected: {},
}))((props: { label: string }) => <MuiTab disableRipple {...props} />);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  padding: {
    padding: theme.spacing(3),
  },
  demo1: {
    backgroundColor: theme.palette.background.paper,
  },
  demo2: {
    backgroundColor: "#2e1534",
  },
}));

const MenuTabs: React.FC<{
  onChange: (event: any, newValue: number) => void;
  value: number;
}> = ({ onChange, value }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.demo1}>
        <Tabs value={value} onChange={onChange} aria-label="ant example">
          <Tab label="Groups" />
          <Tab label="Profile" />
        </Tabs>
      </div>
    </div>
  );
};

export default MenuTabs;
