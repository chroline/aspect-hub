import "typeface-inter";
import "typeface-clear-sans";
import "typeface-butler";
import "./doka.js";
import "./doka.css";

import React from "react";
import ReactDOM from "react-dom";
import App from "app/index";
import registerServiceWorker from "./serviceWorker";

ReactDOM.render(<App />, document.getElementById("root"));

registerServiceWorker(); // Runs register() as default function
