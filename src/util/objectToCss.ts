import { CSSProperties } from "react";
import OrFalse from "util/OrFalse";

export default (obj: OrFalse<CSSProperties>) => {
  let styles = "";
  Object.entries(obj).forEach(([key, value]) => {
    const formattedKey = key
      .split(/(?=[A-Z])/)
      .map((val) => {
        if (val[0] === val[0].toUpperCase())
          return "-" + val[0].toLowerCase() + val.substring(1, val.length);
        return val;
      })
      .reduce((a, b) => a + b);

    if (value !== undefined && value !== false)
      styles += `${formattedKey}: ${value};`;
  });
  return styles;
};
