export default <R>(fn: () => R) => fn();
