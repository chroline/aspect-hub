const isError = (
  err:
    | Error
    | Partial<{
        name: string;
        message: string;
        code: string;
      }>
) => {
  return (
    err && (err instanceof Error || (err.name && (err.code || err.message)))
  );
};

export default isError;
