export default <L>(item: keyof L | any, list: L): L[keyof L] => {
  if (item in list) return list[item as keyof L];
  else return item;
};
