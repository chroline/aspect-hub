export default (rem: number) => {
  return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
};
