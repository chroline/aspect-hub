import { colors, extendedPalette } from "util/theme/colors";
import { radius, transition } from "util/theme/constants";
import { dimensions } from "util/theme/dimensions";
import { spacings } from "util/theme/spacings";
import { fonts, fontWeights, fontSizes } from "util/theme/typography";

export default {
  colors,
  extendedPalette,
  radius,
  transition,
  dimensions,
  spacings,
  fonts,
  fontWeights,
  fontSizes,
};
