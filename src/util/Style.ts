import { CSSProperties } from "react";
import flattenDeep from "util/flattenDeep";
import objectToCss from "util/objectToCss";

export default abstract class Style<P = any> {
  public static genStyles(arr?: Array<Style>) {
    return flattenDeep(arr || [])
      .map((value) => objectToCss(value.generate()))
      .reduce((a, b) => {
        return a + b;
      }, "");
  }

  public constructor(protected props: P) {}

  public abstract style(): CSSProperties;
  public generate(): CSSProperties {
    const style = this.style();
    return Object.entries(style).reduce<CSSProperties>((a, b) => {
      const [key, value] = b;
      if (!value) return a;
      return {
        ...a,
        [key]: value,
      };
    }, {});
  }
}

export type StyledProps = {
  styles?: Style[];
};
