import { Subscription } from "rxjs";
import { useMount, useUnmount } from "react-use";
import { useState } from "react";

const useSubscription = (subscription: () => Subscription) => {
  const [listener, setListener] = useState<Subscription>();
  useMount(() => {
    setListener(subscription());
  });
  useUnmount(() => {
    console.log("umnount");
    listener?.unsubscribe();
  });
};

export default useSubscription;
