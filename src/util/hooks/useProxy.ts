import { useState } from "react";

const useProxy = <T extends object>(initialValue: T) => {
  const [state, setState] = useState(
    new Proxy(initialValue, {
      get: <P extends keyof T>(target: T, p: P): any => target[p],
      set: <P extends keyof T>(target: T, p: P, value: T[P]): boolean => {
        setState({ ...target, [p]: value }), true;
        return true;
      },
    })
  );
  return state;
};

export default useProxy;
