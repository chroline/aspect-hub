export default interface Post {
  id: string;
  author: string;
  type: "text" | "announcement" | "image" | "video";
  content: string;
  caption?: string;
  capsule: PostCapsuleData | null;
  date: firebase.firestore.Timestamp;
  likes: string[];
}

export interface PostCapsuleData {
  label: string;
  value: string;
}
