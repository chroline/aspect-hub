export default interface User {
  pfp: string | null;
  groups: string[];
  name: string;
  email: string;
  id: string;
}
