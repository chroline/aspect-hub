export default interface Group {
  capsules: { [k: string]: string };
  members: string[];
  name: string;
  id: string;
}
