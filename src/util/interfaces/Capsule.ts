export default interface Capsule {
  id: string;
  name: string;
  lock?: firebase.firestore.Timestamp;
}
