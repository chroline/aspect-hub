export default interface Person {
  data: { name: string; id: string };
}
