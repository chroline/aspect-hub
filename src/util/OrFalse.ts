type OrFalse<T> = {
  [K in keyof T]: T[K] | false;
};

export default OrFalse;
