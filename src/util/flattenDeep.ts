const flattenDeep = <T>(arr: Array<T> | T): Array<T> =>
  Array.isArray(arr)
    ? arr.reduce<Array<T>>((a, b) => a.concat(flattenDeep(b)), [] as Array<T>)
    : [arr];

export default flattenDeep;
