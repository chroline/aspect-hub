export default <P>(p: keyof P) => (ps: P) => ps[p];
