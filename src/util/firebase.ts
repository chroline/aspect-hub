import { map } from "rxjs/operators";
import { combineLatest, from } from "rxjs";
import iife from "util/iife";

/**
 *
 * @param config firebase configuration
 */
const load = (config: any) => {
  const app$ = from(import("firebase/app"));
  const firestore$ = from(import("firebase/firestore"));
  const auth$ = from(import("firebase/auth"));
  const rxfire$ = from(import("rxfire/firestore"));

  let persistant = false;

  return combineLatest([app$, firestore$, auth$, rxfire$]).pipe(
    map(([firebase, , rxfire]) => {
      const app = firebase.apps[0] || firebase.initializeApp(config);
      !persistant &&
        iife(() => {
          persistant = true;
          //firebase.firestore().enablePersistence();
        });

      return {
        app,
        rxfire,
      };
    })
  );
};

export default load({
  apiKey: "AIzaSyCVgh9ZXzFPhUwZ8ub-OqSmfoi1eF8V5RY",
  authDomain: "aspect-app.firebaseapp.com",
  databaseURL: "https://aspect-app.firebaseio.com",
  projectId: "aspect-app",
  storageBucket: "aspect-app.appspot.com",
  messagingSenderId: "527442382970",
  appId: "1:527442382970:web:2cff2821b58ff99da8abc3",
  measurementId: "G-06Q8TYPC63",
});
