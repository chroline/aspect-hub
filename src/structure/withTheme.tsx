import React from "react";
import themeBase from "util/theme";
import { css, jsx, SerializedStyles } from "@emotion/core";
import iife from "util/iife";

type Theme = typeof themeBase;

const withTheme = <ExtraProps extends { [k: string]: any }>(
  themeFn: (theme: Theme, props: ExtraProps) => SerializedStyles
) => <
  T extends keyof JSX.IntrinsicElements | React.JSXElementConstructor<any>,
  Q = JSX.LibraryManagedAttributes<T, React.ComponentPropsWithRef<T>>
>(
  Component: T,
  noop?: (keyof ExtraProps)[]
) =>
  React.forwardRef<
    unknown,
    Q &
      ExtraProps & {
        className?: any;
        css?: SerializedStyles;
      }
  >((props, ref) => {
    return (
      <>
        {jsx(
          Component,
          Object.entries({
            ...props,
            ...noop?.reduce((a, b) => ({ ...a, [b]: undefined }), {}),
            className: props.className,
            css: css`
              ${themeFn(themeBase, props)} ${props.css}
            `,
            ref,
          }).reduce(
            (a, b) => ({
              ...a,
              ...iife(() => (noop?.includes(b[0]) ? {} : { [b[0]]: b[1] })),
            }),
            {}
          )
        )}
      </>
    );
  });

export default withTheme;
