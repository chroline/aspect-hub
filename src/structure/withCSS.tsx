import React, { MutableRefObject, PropsWithChildren } from "react";
import themeBase from "util/theme";
import { jsx, SerializedStyles } from "@emotion/core";

export type Theme = typeof themeBase;

const withCSS = <
  ExtraProps extends { [k: string]: any },
  T extends React.JSXElementConstructor<
    ExtraProps & {
      ref: ((instance: unknown) => void) | MutableRefObject<any> | null;
      theme: Theme;
    }
  > = React.FC<
    ExtraProps & {
      ref: ((instance: unknown) => void) | MutableRefObject<any> | null;
      theme: Theme;
    }
  >
>(
  Component: T
) =>
  React.forwardRef<
    unknown,
    PropsWithChildren<
      ExtraProps & {
        className?: any;
        css?: SerializedStyles;
      }
    >
  >((props, ref) => {
    return (
      <>
        {jsx(Component, {
          ...props,
          theme: themeBase,
          ref,
        })}
      </>
    );
  });

export default withCSS;
