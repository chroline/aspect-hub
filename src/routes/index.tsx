import { Redirect, Route, Switch, useLocation } from "react-router-dom";
import AuthRouter from "routes/auth";
import TabsRouter from "./tabs";
import LoadingScreen from "components/organisms/LoadingScreen";
import React, { useState } from "react";
import { useAuthenticationFlow } from "ctrl/AuthenticationFlow";
import AuthProvider from "ctrl/Auth";
import { doc } from "rxfire/firestore";
import Firebase from "store/Firebase";
import { useMount, useUnmount } from "react-use";
import Page from "views/auth/page";
import { Subscription } from "rxjs";

const Router = () => {
  const location = useLocation();
  const [{ matches }, send] = useAuthenticationFlow();

  const [auth$, setAuth$] = useState<Subscription>();
  useMount(() => {
    const db = Firebase.app.firestore();
    setAuth$(
      AuthProvider.user$().subscribe((user) => {
        if (user === null) send("IDLE");
        else {
          const userDoc = db.doc(`users/${AuthProvider.user()?.uid}`);
          doc(userDoc).subscribe((snapshot) => {
            if (snapshot.exists) send("COMPLETE");
            else send("LOADING");
          });
        }
      })
    );
  });
  useUnmount(() => auth$?.unsubscribe());

  return (
    <>
      {!matches("loading") && (
        <Switch location={location} key={"main"}>
          {matches("idle") && location.pathname.indexOf("/auth") !== 0 && (
            <Page>
              <Redirect to={"/auth"} />
            </Page>
          )}
          <Route path="/auth" component={AuthRouter} />
          <Route path="/" component={TabsRouter} />
        </Switch>
      )}
      {matches("loading") && <LoadingScreen key={"loading"} />}
    </>
  );
};

export default Router;
