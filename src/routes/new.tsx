import React, { useEffect, useState } from "react";
import NewTextPost from "views/newPost/text";
import { useRouteMatch } from "react-router-dom";
import NewAnnouncementPost from "views/newPost/announcement";
import NewMediaPost from "views/newPost/media";

const NewRouter = () => {
  const match = useRouteMatch<{
    page: string;
  }>("/new/:page");

  const [page, setPage] = useState(match?.params.page);

  useEffect(() => {
    match?.params.page && setPage(match?.params.page);
  }, [match?.params.page]);

  return (
    <>
      {page === "text" && <NewTextPost />}
      {page === "announcement" && <NewAnnouncementPost />}
      {page === "media" && <NewMediaPost />}
    </>
  );
};

export default NewRouter;
