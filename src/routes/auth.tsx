import {
  Link,
  Redirect,
  Route,
  Switch,
  useLocation,
  useRouteMatch,
} from "react-router-dom";
import React from "react";
import Login from "views/auth/login";
import { AnimatePresence, motion } from "framer-motion";
import Page from "views/auth/page";
import Register from "views/auth/register";
import { useAuthenticationFlow } from "ctrl/AuthenticationFlow";
import CreateAccount from "views/auth/createAccount";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";

const Wrapper = withTheme(
  () => css`
    display: flex;
    flex-direction: column;
    height: 100%;
    overflow: hidden;
    width: 100%;
  `
)(motion.div);

const BottomDisplay = withTheme(
  ({ colors, spacings, fontSizes, fontWeights }) => css`
    align-items: center;
    box-sizing: content-box;
    display: flex;
    height: ${spacings[20]};
    justify-content: center;
    padding-bottom: env(safe-area-inset-bottom);
    width: 100%;
    a {
      color: black;
      font-size: ${fontSizes.lg};
      text-decoration: none;
      b {
        color: ${colors.primary};
        font-weight: ${fontWeights.medium};
      }
    }
  `
)("div");

const AuthRouter = () => {
  const location = useLocation();
  const match = useRouteMatch<{
    page: string;
  }>("/auth/:page");
  const [{ matches }] = useAuthenticationFlow();

  return (
    <Wrapper initial={{}} exit={{}}>
      {
        <AnimatePresence exitBeforeEnter>
          {matches("idle") && (
            <Switch location={location} key={location.pathname}>
              <Route path={"/auth/register"} component={Register} />
              <Route path={"/auth/login"} component={Login} />
              <Route path={"/auth"}>
                <Page>
                  <Redirect to="/auth/register" />
                </Page>
              </Route>
            </Switch>
          )}
          {matches("complete") && <Redirect to="/" />}
          {matches("getPwd") && <CreateAccount />}
        </AnimatePresence>
      }
      {matches("idle") && (
        <BottomDisplay>
          {match?.params.page !== "login" ? (
            <Link to={"/auth/login"}>
              Already have an account? <b>Sign in</b>
            </Link>
          ) : (
            <Link to={"/auth/register"}>
              Don't have an account? <b>Sign up</b>
            </Link>
          )}
        </BottomDisplay>
      )}
      <style jsx>
        {`
          :global(html, body, #root, :root) {
            overflow: hidden;
          }
        `}
      </style>
    </Wrapper>
  );
};

export default AuthRouter;
