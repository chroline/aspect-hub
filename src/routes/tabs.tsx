import React, { useEffect, useRef, useState } from "react";
import NavBar from "components/molecules/NavBar";
import NoGroup from "views/noGroup";
import { useViewReducer } from "ctrl/View";
import MenuPage from "views/menu";
import { AnimatePresence } from "framer-motion";
import TabBar from "components/molecules/TabBar";
import { GroupDataProvider, useGroupData } from "store/GroupData";
import Bulletin from "views/tabs/bulletin";
import { useHistory, useLocation, useRouteMatch } from "react-router-dom";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { useLockBodyScroll, useScroll } from "react-use";
import NewRouter from "routes/new";
import Capsules from "views/tabs/capsules";
import CapsuleView from "views/capsule";
import PostModal from "components/organisms/PostModal";
import { Menu as MenuIcon } from "react-feather";
import Settings from "views/tabs/settings";

const View = withTheme(
  () => css`
    height: 100%;
    overflow: scroll;
    width: 100%;
  `
)("div");

const GroupsBtn = withTheme(
  ({ colors, fontWeights }) => css`
    background: none;
    border: none;
    color: ${colors.primary};
    font-weight: ${fontWeights.bold};
    text-decoration: none;
    padding: 0;
  `
)("button");

const TabsRouter = () => {
  const history = useHistory();
  const location = useLocation();
  const [, send] = useViewReducer();
  const [{ tab, showMenu }, methods] = useViewReducer();
  const [groupID] = useGroupData();
  const [post, setPost] = useState<string>();

  const pageMatch = useRouteMatch<{
    page: string;
    post: string;
  }>("/:page");
  const postMatch = useRouteMatch<{
    post: string;
  }>("/post/:post");

  useEffect(() => {
    if (pageMatch?.params.page !== "menu") send.TOGGLE_MENU(false);
    else send.TOGGLE_MENU(true);

    switch (pageMatch?.params.page) {
      case "capsules":
        methods.CHANGE_TABS("capsules");
        break;
      case "settings":
        methods.CHANGE_TABS("settings");
        break;
      case "new":
        methods.CHANGE_TABS("new");
        break;
      case "capsule":
        methods.CHANGE_TABS("capsule");
        break;
      case "bulletin":
        methods.CHANGE_TABS("bulletin");
        break;
      case "menu":
        methods.CHANGE_TABS("bulletin");
        break;
      case null:
      case undefined:
        history.replace("/bulletin");
    }

    setPost(postMatch?.params.post);
  }, [pageMatch?.params.page]);

  useLockBodyScroll(showMenu);

  const scrollRef = useRef<HTMLElement>(null);
  const { y: scroll } = useScroll(scrollRef);

  return (
    <View ref={scrollRef}>
      <NavBar
        title={tab ? tab.substring(0, 1).toUpperCase() + tab.substring(1) : ""}
        scroll={scroll}
      >
        <GroupsBtn onClick={() => history.push("/menu", true)}>
          <MenuIcon />
        </GroupsBtn>
      </NavBar>

      {!groupID && <NoGroup />}

      <AnimatePresence initial={false}>
        {groupID && !showMenu && tab === "bulletin" && (
          <Bulletin scroll={scroll} key={"bulletin"} />
        )}
        {groupID && !showMenu && tab === "capsules" && (
          <Capsules scroll={scroll} key={"capsules"} />
        )}
        {groupID && !showMenu && tab === "settings" && (
          <Settings scroll={scroll} key={"capsule-router"} />
        )}
        {groupID && !showMenu && tab === "new" && (
          <NewRouter key={"new-router"} />
        )}
        {groupID && !showMenu && tab === "capsule" && (
          <CapsuleView key={"capsule-router"} />
        )}
      </AnimatePresence>

      {!!groupID && <TabBar />}

      <PostModal
        open={!!post}
        close={() =>
          location.state ? history.goBack() : history.replace("/bulletin")
        }
        postID={post}
      />

      <AnimatePresence>{showMenu && <MenuPage />}</AnimatePresence>
    </View>
  );
};

export default () => (
  <GroupDataProvider>
    <TabsRouter />
  </GroupDataProvider>
);
