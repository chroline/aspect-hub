import Firebase from "store/Firebase";
import React, { useState } from "react";
import { AuthenticationFlowProvider } from "ctrl/AuthenticationFlow";
import Router from "routes";
import { BrowserRouter } from "react-router-dom";
import withCSS, { Theme } from "structure/withCSS";
import { ViewStateProvider } from "ctrl/View";
import NotificationsViewer from "components/organisms/NotificationsViewer";
import { MediaCacheProvider } from "store/MediaCache";

const App: React.FC<{ theme: Theme }> = ({ theme }) => {
  const [firebase, setFirebase] = useState(false);

  Firebase.load().subscribe((_) => setFirebase(true));

  return firebase ? (
    <AuthenticationFlowProvider>
      <NotificationsViewer />
      <ViewStateProvider>
        <BrowserRouter>
          <Router />
        </BrowserRouter>
      </ViewStateProvider>
      <style jsx>{`
        :global(html, body, #root, :root) {
          background: ${theme.colors.muted};
          box-sizing: border-box;
          font-family: ${theme.fonts["sans-serif"]};
          font-size: 0.875rem;
          height: 100%;
          margin: 0;
          width: 100%;
        }
        :global(*, *:before, *:after, *:hover, *:active, *:focus) {
          box-sizing: border-box;
          outline: none;
          -webkit-tap-highlight-color: transparent;

          -webkit-appearance: none;
          -moz-appearance: none;
          appearance: none;
        }
        :global(svg) {
          display: block;
        }
      `}</style>
    </AuthenticationFlowProvider>
  ) : (
    <></>
  );
};

export default withCSS(({ theme }) => (
  <MediaCacheProvider>
    <App theme={theme} />
  </MediaCacheProvider>
));
