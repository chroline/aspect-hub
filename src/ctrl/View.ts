import constate from "constate";
import { useMethods } from "react-use";

interface ViewState {
  tab: "bulletin" | "capsules" | "settings" | "new" | "capsule" | null;
  showMenu: boolean;
}

const initialState: () => ViewState = () => {
  return {
    tab: "bulletin",
    showMenu: false,
  };
};

const methods = (state: ViewState) => ({
  CHANGE_TABS(tab: "bulletin" | "capsules" | "settings" | "new" | "capsule") {
    return { ...state, tab };
  },
  TOGGLE_MENU(showMenu: boolean) {
    return { ...state, showMenu };
  },
});

const factory = () => useMethods(methods, initialState());

const [ViewStateProvider, useViewReducer] = constate(factory);

export { ViewStateProvider, useViewReducer };
