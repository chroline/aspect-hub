import { assign, Machine } from "xstate";
import constate from "constate";
import { useMachine } from "@xstate/react/lib";

interface AuthenticationStateSchema {
  states: {
    idle: {};
    getPwd: {};
    loading: {};
    complete: {};
  };
}

interface AuthenticationContext {
  name: string;
  email: string;
  code: string;
  pwd: string;
}

const AuthenticationFlow = Machine<
  AuthenticationContext,
  AuthenticationStateSchema,
  { type: string; value: string }
>({
  key: "auth",
  initial: "loading",
  context: {
    name: "",
    email: "",
    code: "",
    pwd: "",
  },
  states: {
    idle: {
      on: {
        LOGIN: "complete",
        REGISTER: "getPwd",
      },
    },
    getPwd: {
      on: {
        NEXT: "loading",
      },
    },
    loading: {
      on: {
        IDLE: "idle",
        COMPLETE: "complete",
      },
    },
    complete: {
      on: {
        RESET: "idle",
        LOADING: "loading",
      },
    },
  },
  on: {
    INPUT_NAME: {
      actions: assign((ctx, event) => {
        return { ...ctx, name: event.value };
      }),
    },
    INPUT_EMAIL: {
      actions: assign((ctx, event) => {
        return { ...ctx, email: event.value };
      }),
    },
    INPUT_CODE: {
      actions: assign((ctx, event) => {
        return { ...ctx, code: event.value };
      }),
    },
    INPUT_PWD: {
      actions: assign((ctx, event) => {
        return { ...ctx, pwd: event.value };
      }),
    },

    IDLE: "idle",
    LOADING: "loading",
  },
});

export const [
  AuthenticationFlowProvider,
  useAuthenticationFlow,
] = constate(() => useMachine(AuthenticationFlow));
