import User from "util/interfaces/User";
import Firebase from "store/Firebase";
import { from, of } from "rxjs";
import { catchError } from "rxjs/operators";
import Notifications from "store/Notifications";
import { FirebaseError } from "firebase/app";
import isError from "util/lambda/isError";

namespace Users {
  export const create = (uid: string, email: string, name: string) =>
    from(
      Firebase.app.firestore().collection("users").doc(uid).set({
        name,
        email,
        group: [],
        pfp: null,
      })
    );

  export const update = (uid: string, data: Partial<User>) => {
    const db = Firebase.app.firestore();
    const updater = from(db.doc(`users/${uid}`).update(data)).pipe(
      catchError((err) => of(err as FirebaseError))
    );
    updater.subscribe((value) => {
      let err = value as FirebaseError;
      if (isError(err)) {
        Notifications.push({
          type: "warning",
          title: "Error: ",
          desc: "an error occurred while creating a new post",
        });
      }
    });
    return updater;
  };
}

export default Users;
