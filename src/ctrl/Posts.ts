import AuthProvider from "ctrl/Auth";
import Firebase from "store/Firebase";
import { forkJoin, from, of } from "rxjs";
import { catchError } from "rxjs/operators";
import Notifications from "store/Notifications";
import { FirebaseError } from "firebase/app";
import isError from "util/lambda/isError";
import Post, { PostCapsuleData } from "util/interfaces/Post";
import Capsules from "ctrl/Capsules";

namespace Posts {
  export const create = (
    groupID: string,
    type: "text" | "announcement" | "image" | "video",
    data: { content: string; caption?: string }
  ) => {
    const postSchema = {
      author: AuthProvider.user()?.uid,
      date: new Date(),
      type,
      ...data,
      likes: [],
      capsule: "",
    };
    const db = Firebase.app.firestore();

    const creator = from(
      db.collection(`groups/${groupID}/posts`).add(postSchema)
    ).pipe(catchError((err: Error) => of(err)));
    creator.subscribe((value) => {
      let err = value as FirebaseError;
      if (isError(err)) {
        Notifications.push({
          type: "warning",
          title: "Error: ",
          desc: "an error occurred while creating a new post",
        });
      } else {
        Notifications.push({
          type: "success",
          title: "Post created!",
          desc: "",
        });
      }
    });
    return creator;
  };

  export const remove = (post: Post, groupID: string) => {
    const db = Firebase.app.firestore();

    return forkJoin([
      from(db.doc(`groups/${groupID}/posts/${post.id}`).delete()),
      Capsules.removePost(
        (post.capsule as PostCapsuleData).value,
        post.id,
        groupID
      ),
    ]).pipe(catchError((err: Error) => of(err)));
  };

  export const like = (id: string, groupID: string, likes: string[]) => () => {
    const db = Firebase.app.firestore();
    const postDoc = db.doc(`groups/${groupID}/posts/${id}`);

    likes.includes(AuthProvider.user()?.uid || "")
      ? likes.splice(likes.indexOf(AuthProvider.user()?.uid || ""), 1)
      : likes.push(AuthProvider.user()?.uid || "");

    return from(
      postDoc.update({
        likes,
      })
    );
  };
}

export default Posts;
