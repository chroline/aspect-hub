import Firebase from "store/Firebase";
import { from, of } from "rxjs";
import { firestore } from "firebase/app";
import { catchError } from "rxjs/operators";
import Group from "util/interfaces/Group";

namespace Groups {
  export const create = (name: string, uid: string) =>
    from(
      Firebase.app
        .firestore()
        .collection(`groups`)
        .add({
          name,
          members: [uid],
          capsules: {},
        } as Omit<Group, "id">)
    );

  export const changeName = (name: string, groupID: string) =>
    from(
      Firebase.app.firestore().doc(`groups/${groupID}`).update({ name: name })
    );

  export const addMember = (email: string, groupID: string) =>
    from(
      Firebase.app
        .firestore()
        .collection(`users`)
        .where("email", "==", email)
        .get()
        .then((u) => {
          if (u.docs.length !== 1) throw new Error();
          else
            return Firebase.app
              .firestore()
              .doc(`groups/${groupID}`)
              .update({
                members: firestore.FieldValue.arrayUnion(u.docs[0].id),
              });
        })
    ).pipe(catchError((err) => of(err as Error)));

  export const removeMember = (id: string, groupID: string) =>
    from(
      Firebase.app
        .firestore()
        .doc(`groups/${groupID}`)
        .update({
          members: firestore.FieldValue.arrayRemove(id),
        })
    );
}

export default Groups;
