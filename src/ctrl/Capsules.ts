import Firebase from "store/Firebase";
import { from, of, Subject } from "rxjs";
import { catchError } from "rxjs/operators";
import { FirebaseError, firestore } from "firebase/app";
import isError from "util/lambda/isError";

namespace Capsules {
  export const create = (name: string, groupID: string) => {
    const db = Firebase.app.firestore();
    const collection = db.collection(`groups/${groupID}/capsules`);

    const subject = new Subject<firebase.firestore.DocumentReference | Error>();

    const creator = from(
      collection.add({
        name,
        posts: [],
      })
    );
    creator.pipe(catchError((err) => of(err as Error))).subscribe((value) => {
      let err = value as FirebaseError;
      if (isError(err)) return subject.next(value as Error);
      from(
        db.doc(`groups/${groupID}`).update({
          [`capsules.${name}`]: (value as firebase.firestore.DocumentReference)
            .id,
        })
      )
        .pipe(catchError((err) => of(err as Error)))
        .subscribe((res) => {
          let err = res as FirebaseError;
          if (isError(err)) return subject.next(err);
          subject.next(value);
        });
    });
    return subject;
  };

  export const removePost = (
    capsuleID: string,
    postID: string,
    groupID: string
  ) =>
    from(
      Firebase.app
        .firestore()
        .doc(`groups/${groupID}/capsules/${capsuleID}`)
        .update({
          posts: firestore.FieldValue.arrayRemove(
            `groups/${groupID}/posts/${postID}`
          ),
        })
    );

  export const addPost = (capsuleID: string, postID: string, groupID: string) =>
    from(
      Firebase.app
        .firestore()
        .doc(`groups/${groupID}/capsules/${capsuleID}`)
        .update({
          posts: firestore.FieldValue.arrayUnion(
            `groups/${groupID}/posts/${postID}`
          ),
        })
    );

  export const update = (
    capsuleID: string,
    data: Partial<{
      name: string;
      lock: Date | null;
    }>,
    groupID: string
  ) =>
    from(
      Firebase.app
        .firestore()
        .doc(`groups/${groupID}/capsules/${capsuleID}`)
        .update(data)
    );

  export const remove = (capsuleID: string, groupID: string) =>
    from(
      Firebase.app
        .firestore()
        .doc(`groups/${groupID}/capsules/${capsuleID}`)
        .delete()
    );
}

export default Capsules;
