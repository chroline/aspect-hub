import { forkJoin, from } from "rxjs";
import { map } from "rxjs/operators";
import iife from "util/iife";
import * as firebase from "firebase/app";

const config = {
  apiKey: "AIzaSyBuIndwhJ_UkwyYKlXW0waekiyzTxS8pj8",
  authDomain: "aspect-hub.firebaseapp.com",
  databaseURL: "https://aspect-hub.firebaseio.com",
  projectId: "aspect-hub",
  storageBucket: "aspect-hub.appspot.com",
  messagingSenderId: "1057074650178",
  appId: "1:1057074650178:web:b39ae10324491db06261db",
  measurementId: "G-52GRW5TQZV",
};

export default class Firebase {
  static app: firebase.app.App;

  private static persistent: boolean = false;

  static load() {
    const app$ = from(import("firebase/app"));
    const auth$ = from(import("firebase/auth"));
    const firestore$ = from(import("firebase/firestore"));
    const storage$ = from(import("firebase/storage"));

    const observable = forkJoin([app$, auth$, firestore$, storage$]).pipe(
      map(([firebase]) => {
        const app = firebase.apps[0] || firebase.initializeApp(config);
        !this.persistent &&
          iife(() => {
            this.persistent = true;
            firebase.firestore().enablePersistence({ synchronizeTabs: true });
          });

        return { app };
      })
    );

    observable.subscribe(({ app }) => {
      this.app = app;
    });

    return observable;
  }
}
