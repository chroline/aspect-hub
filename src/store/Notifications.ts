import { BehaviorSubject } from "rxjs";

export interface NotificationSchema {
  title: string;
  desc?: string;
  type: "success" | "error" | "warning" | "info";
}

namespace Notifications {
  export const Collection = new BehaviorSubject<
    (NotificationSchema & { id: number })[]
  >([]);

  export const push = (value: NotificationSchema) => {
    const notif = { ...value, id: new Date().getMilliseconds() };
    Collection.next([...Collection.getValue(), notif]);
    setTimeout(() => remove(notif), 7000);
  };
  export const remove = (value: NotificationSchema & { id: number }) => {
    const col = Array.from(Collection.getValue());
    col.splice(col.indexOf(value), 1);
    Collection.next(col);
  };
}

export default Notifications;
