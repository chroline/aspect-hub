import { useMap } from "react-use";
import { from, of, ReplaySubject } from "rxjs";
import Firebase from "store/Firebase";
import { getDownloadURL } from "rxfire/storage";
import { catchError } from "rxjs/operators";
import { FirebaseError } from "firebase/app";
import isError from "util/lambda/isError";
import constate from "constate";
import ky from "ky";

const MediaCacheFactory = () => {
  const [mediaCacheMap, { get: mapGet, set: mapSet }] = useMap<{
    [k: string]: ReplaySubject<string>;
  }>();

  const get = (path: string) => {
    return mapGet(path) || add(path);
  };
  const add = (path: string) => {
    const storage = Firebase.app.storage();
    const subject = new ReplaySubject<string>();
    mapSet(path, subject);
    getDownloadURL(storage.ref(path))
      .pipe(catchError((err) => of(err as Error)))
      .subscribe((res) => {
        let err = res as FirebaseError;
        if (isError(err)) subject.next(undefined);
        else {
          from(ky(res as string).blob()).subscribe((blob: any) =>
            subject.next(URL.createObjectURL(blob))
          );
        }
      });
    return subject;
  };
  const set = (path: string) => {
    const storage = Firebase.app.storage();
    const subject = get(path);
    if (subject) {
      getDownloadURL(storage.ref(path))
        .pipe(catchError((err) => of(err as Error)))
        .subscribe((res) => {
          let err = res as FirebaseError;
          if (isError(err)) subject.next(undefined);
          else {
            from(ky(res as string).blob()).subscribe((blob: any) =>
              subject.next(URL.createObjectURL(blob))
            );
          }
        });
    }
  };

  return [
    mediaCacheMap,
    {
      get,
      add,
      set,
    },
  ] as [
    {
      [k: string]: ReplaySubject<string>;
    },
    { get: typeof get; add: typeof add; set: typeof set }
  ];
};

const [MediaCacheProvider, useMediaCache] = constate(MediaCacheFactory);

export { MediaCacheProvider, useMediaCache };
