import {
  BehaviorSubject,
  combineLatest,
  Observable,
  of,
  Subscription,
  zip,
} from "rxjs";
import { collection, doc } from "rxfire/firestore";
import { catchError, map } from "rxjs/operators";
import isError from "util/lambda/isError";

export class BulletinPostsPaginator<T> {
  private _subject = new BehaviorSubject<
    firebase.firestore.QueryDocumentSnapshot<T>[]
  >([]);

  public get subject() {
    return this._subject;
  }

  private last: BehaviorSubject<number>;

  public get amnt() {
    return (collection(
      this.firebase.collection(this.query).orderBy("date", "desc")
    ) as Observable<firebase.firestore.QueryDocumentSnapshot<T>[]>).pipe(
      map((v) => v.length)
    );
  }

  constructor(
    private firebase: firebase.firestore.Firestore,
    private query: string,
    private limit: number = 25
  ) {
    const sub = new BehaviorSubject<Subscription | null>(null);

    this.last = new BehaviorSubject<number>(limit);
    this.last.subscribe((limit) => {
      sub.getValue()?.unsubscribe();
      sub.next(
        (collection(
          firebase.collection(query).orderBy("date", "desc").limit(limit)
        ) as Observable<firebase.firestore.QueryDocumentSnapshot<T>[]>)
          .pipe(catchError((err) => of(err as Error)))
          .subscribe((docs) => {
            !isError(docs as Error) && this._subject.next(docs as any);
          })
      );
    });
  }

  public next() {
    this.last.next(this.last.getValue() + this.limit);
  }
}

export class CapsulePostsPaginator<T> {
  private _subject = new BehaviorSubject<
    firebase.firestore.QueryDocumentSnapshot<T>[]
  >([]);

  public get subject() {
    return this._subject;
  }

  private list: Observable<
    firebase.firestore.QueryDocumentSnapshot<{ posts: string[] }>
  >;
  private last: BehaviorSubject<number>;

  private _amnt: number = 0;

  public get amnt() {
    return this._amnt;
  }

  constructor(
    private firebase: firebase.firestore.Firestore,
    private query: string,
    private limit: number = 25
  ) {
    this.list = doc(firebase.doc(query)) as Observable<
      firebase.firestore.QueryDocumentSnapshot<{ posts: string[] }>
    >;

    const subs = new BehaviorSubject<Subscription | null>(null);

    this.last = new BehaviorSubject<number>(limit);
    combineLatest([this.list, this.last])
      .pipe(catchError((err) => of(err as Error)))
      .subscribe((res) => {
        let err = res as Error;
        if (!isError(err)) {
          const [list, last] = res as [
            firebase.firestore.QueryDocumentSnapshot<{ posts: string[] }>,
            number
          ];
          const posts = list.data()?.posts || [];
          this._amnt = posts.length;

          const docs$ = [];
          const end = Math.max(posts.length - last, 0);
          for (let i = posts.length - 1; i >= end; i--)
            docs$.push(doc(firebase.doc(posts[i])));

          subs.getValue()?.unsubscribe();

          docs$.length === 0
            ? this.subject.next([])
            : subs.next(
                zip(...docs$).subscribe((value) =>
                  this._subject.next(
                    value as firebase.firestore.QueryDocumentSnapshot<T>[]
                  )
                )
              );
        }
      });
  }

  public next() {
    this.last.next(this.last.getValue() + this.limit);
  }
}
