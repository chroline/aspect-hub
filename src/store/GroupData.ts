import constate from "constate";
import { useEffect, useState } from "react";
import { useMap, useSet } from "react-use";
import { combineLatest, Observable, ReplaySubject, Subscription } from "rxjs";
import Firebase from "store/Firebase";
import { doc } from "rxfire/firestore";
import User from "util/interfaces/User";
import { filter, map } from "rxjs/operators";
import Group from "util/interfaces/Group";
import AuthProvider from "ctrl/Auth";

const savedGroupData = localStorage.getItem("groupData");

const GroupDataFactory = () => {
  const groupData = useState<{ id: string; name: string } | undefined>(
    savedGroupData ? JSON.parse(savedGroupData) : undefined
  );

  const [profileCacheMap, { get: mapGet, set: mapSet }] = useMap<{
    [k: string]: ReplaySubject<firebase.firestore.DocumentSnapshot<User>>;
  }>();
  const [profileList, { reset: resetProfileList }] = useSet<Subscription>();

  useEffect(() => {
    groupData[0] &&
      localStorage.setItem("groupData", JSON.stringify(groupData[0]));
    !groupData[0] && localStorage.removeItem("groupData");

    for (const item of profileList) {
      item.unsubscribe();
    }
    resetProfileList();
  }, [groupData[0]]);

  useEffect(() => {
    const db = Firebase.app.firestore();
    if (groupData[0]?.id) {
      const listener = combineLatest([
        doc(db.doc(`groups/${groupData[0].id}`)) as Observable<
          firebase.firestore.DocumentSnapshot<Group>
        >,
        AuthProvider.user$(),
      ])
        .pipe(
          filter(([, u]) => u !== null),
          map(([v]) => ({ id: v.id, ...v.data() } as Group))
        )
        .subscribe((value) => groupData[1](value));
      return () => listener.unsubscribe();
    }
  }, [groupData[0]?.id]);

  const get = (id: string) => mapGet(id) || add(id);
  const add = (id: string) => {
    const subject = new ReplaySubject<
      firebase.firestore.DocumentSnapshot<User>
    >();
    (Firebase.app
      .firestore()
      .doc(`users/${id}`) as firebase.firestore.DocumentReference<
      User
    >).onSnapshot(
      (snapshot) => {
        subject.next(snapshot);
      },
      () => {}
    );
    mapSet(id, subject);
    return subject;
  };

  return {
    groupData,
    profileCache: [
      profileCacheMap,
      {
        get,
        add,
      },
    ] as [
      {
        [k: string]: ReplaySubject<firebase.firestore.DocumentSnapshot<User>>;
      },
      {
        get: typeof get;
        add: typeof add;
      }
    ],
  };
};

const [GroupDataProvider, useGroupData, useProfileCache] = constate(
  GroupDataFactory,
  (value) => value.groupData,
  (value) => value.profileCache
);

export { GroupDataProvider, useGroupData, useProfileCache };
