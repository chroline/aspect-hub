import React from "react";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { motion } from "framer-motion";
import FolderIcon from "@atlaskit/icon/glyph/folder";
import SettingsIcon from "@atlaskit/icon/glyph/settings";
import HomeIcon from "@atlaskit/icon/glyph/home";
import { useViewReducer } from "ctrl/View";
import { useHistory } from "react-router";

const transition = { duration: 0.3, ease: [0.6, 0.9, 0.1, 1.3] };

const TabBarWrapper = withTheme(
  ({ fontSizes, fontWeights }) => css`
    align-items: center;
    background: white;
    border-top: 1px solid #cacaca;
    border-bottom: 1px solid #cacaca;
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.3);
    bottom: 0;
    display: flex;
    flex-direction: row;
    font-size: ${fontSizes["xl"]};
    font-weight: ${fontWeights.bold};
    height: calc(56px + env(safe-area-inset-bottom));
    justify-content: space-evenly;
    left: 0;
    padding: 0 12px env(safe-area-inset-bottom);
    position: fixed;
    transition: background-color 0.3s ease-in-out, border-top 0.3s ease-in-out;
    width: 100%;
    @supports (
      (-webkit-backdrop-filter: saturate(180%) blur(20px)) or
        (backdrop-filter: saturate(180%) blur(20px))
    ) {
      background: rgba(255, 255, 255, 0.9);
      backdrop-filter: saturate(180%) blur(20px);
    }
  `
)("div");

const Item = withTheme<{ selected?: boolean }>(
  ({ colors, spacings, fontSizes, fontWeights }, { selected }) => css`
    align-items: center;
    background: transparent;
    border: none;
    color: ${selected ? colors.primary : "black"};
    display: flex;
    flex-direction: column;
    width: ${spacings["24"]};
    & > div > span {
      display: block;
    }
    & svg {
      display: block;
      height: ${spacings["8"]};
      //margin-bottom: 2px;
      //margin-top: 6px;
      width: ${spacings["8"]};
    }
    & p {
      font-size: ${fontSizes.sm};
      font-weight: ${fontWeights.medium};
      letter-spacing: 0.03rem;
      //margin-bottom: 6px;
    }
  `
)("button");

const TabBar = () => {
  const [{ tab }] = useViewReducer();
  const history = useHistory();

  return (
    <TabBarWrapper role={"navigation"}>
      <Item
        key={"capsules"}
        onClick={() => history.push("/capsules")}
        selected={tab === "capsules"}
      >
        <motion.div positionTransition={transition} key={"icon"}>
          <FolderIcon label={"capsules"} />
        </motion.div>
        {tab === "capsules" && (
          <motion.p
            key={"desc"}
            initial={{ opacity: 0, y: -2 }}
            animate={{ opacity: 1, y: 0 }}
            exit={{ opacity: 0, y: -2 }}
            transition={transition}
            positionTransition={transition}
          >
            Capsules
          </motion.p>
        )}
      </Item>
      <Item
        key={"bulletin"}
        onClick={() => history.push("/bulletin")}
        selected={tab === "bulletin"}
      >
        <motion.div positionTransition={transition} key={"icon"}>
          <HomeIcon label={"bulletin"} />
        </motion.div>
        {tab === "bulletin" && (
          <motion.p
            key={"desc"}
            initial={{ opacity: 0, y: -2 }}
            animate={{ opacity: 1, y: 0 }}
            exit={{ opacity: 0, y: -2 }}
            transition={transition}
            positionTransition={transition}
          >
            Bulletin
          </motion.p>
        )}
      </Item>
      <Item
        key={"settings"}
        onClick={() => history.push("/settings")}
        selected={tab === "settings"}
      >
        <motion.div positionTransition={transition} key={"icon"}>
          <SettingsIcon label={"settings"} />
        </motion.div>
        {tab === "settings" && (
          <motion.p
            key={"desc"}
            initial={{ opacity: 0, y: -2 }}
            animate={{ opacity: 1, y: 0 }}
            exit={{ opacity: 0, y: -2 }}
            transition={transition}
            positionTransition={transition}
          >
            Settings
          </motion.p>
        )}
      </Item>
    </TabBarWrapper>
  );
};

export default TabBar;
