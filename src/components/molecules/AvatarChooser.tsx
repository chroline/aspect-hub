import React, { useRef, useState } from "react";
import {
  FilePond,
  FilePondProps,
  FilePondServerConfigProps,
  registerPlugin,
} from "react-filepond";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { from } from "rxjs";
import ky from "ky";
import { Box } from "components/atoms";
import withCSS from "structure/withCSS";
import { useMount } from "react-use";
import { useMediaCache } from "store/MediaCache";
import AuthProvider from "ctrl/Auth";
import { useProfileCache } from "store/GroupData";
import useSubscription from "util/hooks/useSubscription";

import "filepond/dist/filepond.min.css";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";
// @ts-ignore
import FilePondPluginImageExifOrientation from "filepond-plugin-image-exif-orientation";
// @ts-ignore
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
// @ts-ignore
import FilePondPluginImageEdit from "filepond-plugin-image-edit";
// @ts-ignore
import FilePondPluginImageTransform from "filepond-plugin-image-edit";
// @ts-ignore
import FilePondPluginImageCrop from "filepond-plugin-image-crop";
// @ts-ignore
import FilePondPluginImageResize from "filepond-plugin-image-resize";
import FilePondPluginFileValidateSize from "filepond-plugin-file-validate-size";
import FilePondPluginFileValidateType from "filepond-plugin-file-validate-type";
// @ts-ignore
import FilePondPluginFileEncode from "filepond-plugin-file-encode";

registerPlugin(
  FilePondPluginFileEncode,
  FilePondPluginFileValidateSize,
  FilePondPluginFileValidateType,
  FilePondPluginImageExifOrientation,
  FilePondPluginImagePreview,
  FilePondPluginImageCrop,
  FilePondPluginImageResize,
  FilePondPluginImageTransform,
  FilePondPluginImageEdit
);

declare var dokaCreate: any;

const Pond = withTheme<
  FilePondProps & {
    allowFileSizeValidation: boolean;
    maxFileSize: any;
    stylePanelLayout: any;
    imagePreviewHeight: any;
    imageCropAspectRatio: any;
    imageResizeTargetWidth: any;
    imageResizeTargetHeight: any;
    styleLoadIndicatorPosition: any;
    styleProgressIndicatorPosition: any;
    styleButtonRemoveItemPosition: any;
    styleButtonProcessItemPosition: any;
    imageEditEditor: any;
    imageEditInstantEdit: any;
  }
>(
  () => css`
    width: 170px;
    margin: 0 auto;
  `
)(FilePond);

const editor = dokaCreate({
  utils: ["crop", "filter", "color"],
});

const AvatarChooser = withCSS<{
  onChange: (file?: File) => void;
}>(({ onChange, theme }) => {
  const [, { get: getProfile }] = useProfileCache();
  const [startingFile, setStartingFile] = useState<{
    source: string;
    options: {
      type: "limbo";
    };
  }>();
  const filepondRef = useRef<HTMLInputElement>();
  const [, { get }] = useMediaCache();
  const [error, setError] = useState<string | undefined>();

  useSubscription(() =>
    AuthProvider.user$().subscribe(
      (user) =>
        user &&
        getProfile(user.uid).subscribe((user) =>
          setStartingFile({
            source: user.data()?.pfp
              ? `pfp/${user.data()?.pfp}`
              : `pfp/default.png`,
            options: {
              type: "limbo",
            },
          })
        )
    )
  );

  const server: FilePondServerConfigProps["server"] = {
    process: (null as unknown) as string,
    revert: (null as unknown) as string,
    restore: (source, load) => {
      get(source).subscribe((res) => {
        console.log(res);
        from(ky(res).blob()).subscribe((blob: any) => {
          setStartingFile(blob);
          load(blob);
          onChange(blob);
        });
      });
      return {
        abort: () => {
          setStartingFile(undefined);
        },
      };
    },
  };

  useMount(() =>
    editor.on("confirm", () => {
      editor
        .getData({
          file: true,
          data: true,
        } as any)
        .then((output: any) => {
          onChange(output.file);
        });
    })
  );

  return (
    <>
      <Box
        css={css`
          flex: 1;
        `}
      >
        <Pond
          acceptedFileTypes={[
            "image/jpeg",
            "image/jpg",
            "image/png",
            "image/gif",
          ]}
          onerror={(error) => {
            setError(error.main);
          }}
          ref={filepondRef}
          files={startingFile ? [startingFile as any] : []}
          allowMultiple={false}
          allowFileSizeValidation
          maxFileSize={"3MB"}
          stylePanelLayout={"compact circle"}
          imagePreviewHeight={100}
          imageCropAspectRatio={"1:1"}
          imageResizeTargetWidth={200}
          imageResizeTargetHeight={200}
          styleLoadIndicatorPosition={"center bottom"}
          styleProgressIndicatorPosition={"right bottom"}
          styleButtonRemoveItemPosition={"left bottom"}
          styleButtonProcessItemPosition={"right bottom"}
          imageEditEditor={editor}
          imageEditInstantEdit={false}
          onupdatefiles={([file]) => {
            setError(undefined);
            onChange(file?.file);
          }}
          server={server}
          instantUpload={false}
        />
      </Box>
      <Box
        css={css`
          align-items: center;
          display: flex;
          flex: 1;
          padding-left: ${theme.spacings["5"]};
          max-width: 200px;
          p {
            font-size: ${theme.fontSizes.lg};
            font-weight: ${theme.fontWeights.medium};
            line-height: 1.4;
          }
        `}
      >
        {error ? (
          <p style={{ color: theme.colors.error }}>{error}</p>
        ) : (
          <p>Choose an avatar that lets people know who you are.</p>
        )}
      </Box>
      <style jsx>{`
        div {
          background: aqua;
          & :global(input) {
            opacity: 0;
            //visibility: hidden;
          }
        }
      `}</style>
    </>
  );
});

export default AvatarChooser;
