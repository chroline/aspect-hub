import iife from "util/iife";
import React, { ChangeEventHandler, useState } from "react";
import { Input } from "components/atoms";
import AuthForm from "components/molecules/AuthForm/index";
import { useAuthenticationFlow } from "ctrl/AuthenticationFlow";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { InputContainer } from "views/auth/pageElements";

const Message = withTheme(
  ({ colors, spacings, fontSizes }) => css`
    color: ${colors.error};
    font-size: ${fontSizes.sm};
    margin: ${spacings[2]} 0;
  `
)("p");

interface Errors {
  name: string | null;
  email: string | null;
}

const validation = (fields: { name: string; email: string }): Errors => {
  const name = iife(() => {
    if (!fields.email || fields.email === "" || fields.email === " ")
      return "Email is required";
    return null;
  });
  const email = iife(() => {
    if (!fields.email || fields.email === "" || fields.email === " ")
      return "Email is required";
    if (!fields.email.match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i))
      return "Invalid email address";
    return null;
  });
  return { name, email };
};

const RegisterForm: React.FC<{
  onSubmit: () => any;
  disabled: boolean;
}> = ({ onSubmit, disabled }) => {
  const [{ context }, send] = useAuthenticationFlow();
  const [errors, setErrors] = useState<Partial<Errors>>({});

  const handleSubmit = () => {
    const errors = validation(context);
    if (errors.name === null && errors.email === null) onSubmit();
    else requestAnimationFrame(() => setErrors(errors));
  };

  const nameChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setErrors((err) => ({ ...err, name: null }));
    send({ type: "INPUT_NAME", value: e.target.value });
  };
  const emailChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setErrors((err) => ({ ...err, email: null }));
    send({ type: "INPUT_EMAIL", value: e.target.value });
  };

  return (
    <AuthForm key={"form"} disabled={disabled} handleSubmit={handleSubmit}>
      <InputContainer>
        <Input
          color={errors.name ? "error" : "primary"}
          name={"name"}
          placeholder={"Name"}
          type={"text"}
          onChange={nameChange}
          value={context.name}
        />
        <Message>{errors.name}</Message>
      </InputContainer>
      <InputContainer>
        <Input
          color={errors.email ? "error" : "primary"}
          name={"email"}
          placeholder={"Email address"}
          type={"email"}
          onChange={emailChange}
          value={context.email}
        />
        <Message>{errors.email}</Message>
      </InputContainer>
    </AuthForm>
  );
};

export default RegisterForm;
