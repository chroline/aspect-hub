import { transition } from "util/theme/constants";
import { motion } from "framer-motion";
import React, { MouseEventHandler } from "react";
import { Button } from "components/atoms";

const AuthForm: React.FC<{
  disabled: boolean;
  handleSubmit: MouseEventHandler<HTMLButtonElement>;
}> = ({ children, disabled, handleSubmit }) => {
  return (
    <motion.div
      key={"form"}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={transition}
      positionTransition={transition}
      style={{ width: "100%" }}
    >
      <div>{children}</div>
      <Button
        block
        color={"primary"}
        disabled={disabled}
        size={"xlarge"}
        type={"submit"}
        onClick={handleSubmit}
      >
        Continue
      </Button>
    </motion.div>
  );
};

export default AuthForm;
