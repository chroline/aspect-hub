import iife from "util/iife";
import React, { ChangeEventHandler, useState } from "react";
import { Input } from "components/atoms";
import AuthForm from "components/molecules/AuthForm/index";
import { useAuthenticationFlow } from "ctrl/AuthenticationFlow";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { InputContainer } from "views/auth/pageElements";

const Message = withTheme(
  ({ colors, spacings, fontSizes }) => css`
    color: ${colors.error};
    font-size: ${fontSizes.sm};
    margin: ${spacings[6]} 0;
  `
)("p");

interface Errors {
  email: string | null;
  pwd: string | null;
}

const validation = (fields: { email: string; pwd: string }): Errors => {
  const email = iife(() => {
    if (!fields.email || fields.email === "" || fields.email === " ")
      return "Email is required";
    if (!fields.email.match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i))
      return "Invalid email address";
    return null;
  });
  const pwd = iife(() => {
    if (!fields.pwd || fields.pwd === "" || fields.pwd === " ")
      return "Password is required";
    if (fields.pwd.length < 6) return "Must be at least 6 characters";
    return null;
  });
  return { email, pwd };
};

const LoginForm: React.FC<{
  onSubmit: () => any;
  disabled: boolean;
}> = ({ onSubmit, disabled }) => {
  const [{ context }, send] = useAuthenticationFlow();

  const [errors, setErrors] = useState<Partial<Errors>>({});

  const handleSubmit = () => {
    const errors = validation(context);
    if (errors.email === null && errors.pwd === null) onSubmit();
    else requestAnimationFrame(() => setErrors(errors));
  };

  const emailChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setErrors((err) => ({ ...err, email: null }));
    send({ type: "INPUT_EMAIL", value: e.target.value });
  };
  const pwdChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setErrors((err) => ({ ...err, pwd: null }));
    send({ type: "INPUT_PWD", value: e.target.value });
  };
  return (
    <AuthForm disabled={disabled} handleSubmit={handleSubmit}>
      <InputContainer>
        <Input
          color={errors.email ? "error" : "primary"}
          name={"email"}
          placeholder={"Email address"}
          type={"email"}
          onChange={emailChange}
          value={context.email}
        />
        <Message>{errors.email}</Message>
      </InputContainer>
      <InputContainer>
        <Input
          color={errors.pwd ? "error" : "primary"}
          name={"password"}
          placeholder={"Password"}
          type={"password"}
          onChange={pwdChange}
          value={context.pwd}
        />
        <Message>{errors.pwd}</Message>
      </InputContainer>
    </AuthForm>
  );
};

export default LoginForm;
