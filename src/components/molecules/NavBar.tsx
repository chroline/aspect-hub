import React from "react";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";

const Left = withTheme(
  () => css`
    flex: 1;
    text-align: left;
  `
)("div");

const Center = withTheme<{ showTitle: boolean }>(
  (_, props) => css`
    flex: 1;
    opacity: ${props.showTitle ? 1 : 0};
    text-align: center;
    transition: all 0.3s ease-in-out;
  `
)("div", ["showTitle"]);

const Right = withTheme(
  () => css`
    flex: 1;
    text-align: center;
  `
)("div");

const NavBarWrapper = withTheme<{ showBg: boolean }>(
  ({ fontSizes, fontWeights }, { showBg }) => css`
    align-items: center;
    background: ${showBg ? "white" : "transparent"};
    border-bottom: 1px solid ${showBg ? "#cacaca" : "transparent"};
    border-top: 1px solid ${showBg ? "#cacaca" : "transparent"};
    display: flex;
    flex-direction: row;
    font-size: ${fontSizes["xl"]};
    font-weight: ${fontWeights.bold};
    height: 56px;
    justify-content: center;
    left: 0;
    padding: 0 12px;
    position: fixed;
    top: 0;
    transition: background-color 0.3s ease-in-out,
      border-bottom 0.3s ease-in-out;
    width: 100%;
    z-index: 2;
    @supports (
      (-webkit-backdrop-filter: saturate(180%) blur(20px)) or
        (backdrop-filter: saturate(180%) blur(20px))
    ) {
      background: ${showBg ? "rgba(255, 255, 255, 0.9)" : "transparent"};
      backdrop-filter: ${showBg ? "saturate(180%) blur(20px)" : "none"};
    }
  `
)("div", ["showBg"]);

const NavBar: React.FC<{
  title: string;
  scroll: number;
}> = ({ title, children, scroll }) => {
  return (
    <>
      <NavBarWrapper showBg={scroll > 0}>
        <Left>{children}</Left>
        <Center showTitle={true}>{title}</Center>
        {/*scroll > remToPx(6) - 22*/}
        <Right />
      </NavBarWrapper>
    </>
  );
};

export default NavBar;
