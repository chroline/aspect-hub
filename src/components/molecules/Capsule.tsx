import React from "react";
import withCSS from "structure/withCSS";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { Folder } from "react-feather";
import { useHistory } from "react-router";

const Icon = withTheme(
  (theme) => css`
    color: ${theme.colors.primary};
    margin-right: ${theme.spacings["5"]};
  `
)(Folder);
const Capsule = withCSS<{ name: string; id: string }>(({ theme, name, id }) => {
  const history = useHistory();

  const click = () => history.push("/capsule/" + id);

  return (
    <>
      <button className={"capsule"} onClick={click}>
        <div className={"content"}>
          <Icon />
          {name}
        </div>
      </button>
      <style jsx>{`
        .capsule {
          background: none;
          border: none;
          padding: ${theme.spacings["2"]};
          width: 100%;
        }
        .content {
          align-items: center;
          background: white;
          border: 1px solid #dfdfdf;
          border-radius: ${theme.radius};
          box-shadow: 0 1px 3px rgba(0, 0, 0, 0.02),
            0 2px 1px rgba(0, 0, 0, 0.012), 0 1px 1px rgba(0, 0, 0, 0.014);
          display: flex;
          font-size: ${theme.fontSizes.lg};
          font-weight: ${theme.fontWeights.medium};
          line-height: 1.4;
          padding: ${theme.spacings["5"]};
        }
      `}</style>
    </>
  );
});

export default Capsule;
