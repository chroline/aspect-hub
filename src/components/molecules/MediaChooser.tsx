import React, { useRef, useState } from "react";
import {
  FilePond,
  FilePondProps,
  FilePondServerConfigProps,
  registerPlugin,
} from "react-filepond";

import "filepond/dist/filepond.min.css";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";
import "filepond-plugin-media-preview/dist/filepond-plugin-media-preview.min.css";
// @ts-ignore
import FilePondPluginImageExifOrientation from "filepond-plugin-image-exif-orientation";
// @ts-ignore
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
// @ts-ignore
import FilePondPluginImageEdit from "filepond-plugin-image-edit";
// @ts-ignore
import FilePondPluginImageTransform from "filepond-plugin-image-edit";
// @ts-ignore
import FilePondPluginImageCrop from "filepond-plugin-image-crop";
// @ts-ignore
import FilePondPluginImageResize from "filepond-plugin-image-resize";
import FilePondPluginFileValidateSize from "filepond-plugin-file-validate-size";
import FilePondPluginFileValidateType from "filepond-plugin-file-validate-type";
// @ts-ignore
import FilePondPluginFileEncode from "filepond-plugin-file-encode";

import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import withCSS from "structure/withCSS";
import { useMount } from "react-use";
// @ts-ignore
import FilePondPluginMediaPreview from "filepond-plugin-media-preview";

registerPlugin(
  FilePondPluginFileEncode,
  FilePondPluginFileValidateSize,
  FilePondPluginFileValidateType,
  FilePondPluginImageExifOrientation,
  FilePondPluginImagePreview,
  FilePondPluginImageCrop,
  FilePondPluginImageResize,
  FilePondPluginImageTransform,
  FilePondPluginImageEdit,
  FilePondPluginMediaPreview
);

declare var dokaCreate: any;

const Pond = withTheme<
  FilePondProps & {
    allowFileSizeValidation: boolean;
    maxFileSize: any;
    imageEditEditor: any;
    imageEditInstantEdit: any;
  }
>(
  () => css`
    margin: 0 auto;
    & .filepond--panel-root {
      background: #fafbfc;
      border: 2px solid #dfdfdf;
    }
  `
)(FilePond);

const editor = dokaCreate({
  utils: ["crop", "filter", "color"],
});

const MediaChooser = withCSS<{
  onChange: (file?: File) => void;
}>(({ onChange, theme }) => {
  const fileRef = useRef<any>();
  const filepondRef = useRef<HTMLInputElement>();

  const [, setError] = useState<string | undefined>();

  const server: FilePondServerConfigProps["server"] = {
    process: (null as unknown) as string,
    revert: (null as unknown) as string,
  };

  useMount(() =>
    editor.on("confirm", () => {
      editor
        .getData({
          file: true,
          data: true,
        } as any)
        .then((output: any) => {
          fileRef.current = output.file;
          onChange(output.file);
        });
    })
  );

  return (
    <Pond
      allowFileSizeValidation
      maxFileSize={"20MB"}
      acceptedFileTypes={[
        "image/jpeg",
        "image/jpg",
        "image/png",
        "image/gif",
        "video/*",
      ]}
      onerror={(error) => {
        setError(error.main);
      }}
      ref={filepondRef}
      files={[]}
      allowMultiple={false}
      imageEditEditor={editor}
      imageEditInstantEdit={false}
      onupdatefiles={([file]) => {
        setError(undefined);
        fileRef.current = file?.file;
        onChange(file?.file);
      }}
      server={server}
      instantUpload={false}
    />
  );
});

export default MediaChooser;
