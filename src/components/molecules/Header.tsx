import withTheme from "structure/withTheme";
import { css } from "@emotion/core";

const Header = withTheme(
  ({ colors, fonts, fontWeights, fontSizes, spacings }) => css`
    background: ${colors.muted};
    display: flex;
    padding-bottom: ${spacings["8"]};
    padding-top: ${spacings["24"]};
    justify-content: center;
    position: relative;
    text-align: center;
    width: 100%;
    h1 {
      font-family: ${fonts.serif};
      font-weight: ${fontWeights.bold};
      font-size: ${fontSizes["5xl"]};
    }
    h2 {
      font-family: ${fonts["sans-serif"]};
      font-size: ${fontSizes.xl};
      margin-top: ${spacings[4]};
    }
  `
)("div");

export default Header;
