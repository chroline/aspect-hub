import React, { useEffect, useState } from "react";
import withCSS from "structure/withCSS";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { ThumbsUp } from "react-feather";
import { useGroupData, useProfileCache } from "store/GroupData";
import useSubscription from "util/hooks/useSubscription";
import moment from "moment";
import Ink from "react-ink";
import AuthProvider from "ctrl/Auth";
import Posts from "ctrl/Posts";
import clsx from "clsx";
import { useHistory } from "react-router";
import { useMediaCache } from "store/MediaCache";

const Avatar = withTheme(
  ({ spacings }) =>
    css`
      align-items: center;
      border-radius: 50%;
      display: flex;
      height: 3rem;
      justify-content: center;
      margin-right: ${spacings["4"]};
      overflow: hidden;
      width: 3rem;
    `
)("div");

const ImgAvatar = withTheme(
  () => css`
    object-fit: cover;
    height: 3rem;
    width: 100%;
  `
)("img");

export const _Post = withCSS<{
  title: string;
  desc?: string;
  time: string;
  avatar?: JSX.Element;
  id: string;
  likes: string[];
  liked: boolean;
  modal?: boolean;
}>(
  ({ children, title, desc, time, theme, avatar, id, likes, liked, modal }) => {
    const [groupData] = useGroupData();
    const history = useHistory();

    return (
      <>
        <div
          className={clsx("post", modal ? "" : "no-modal")}
          onClick={() => modal && history.push("/post/" + id, true)}
        >
          <Avatar>{avatar}</Avatar>
          <div className={"content"}>
            <h3>{title}</h3>
            <p>
              {desc}
              {desc ? <span>⋅</span> : ""}
              <b>{time}</b>
            </p>
            {children}
            <div className={"likes"}>
              <button
                onClick={(e) => {
                  e.stopPropagation();
                  Posts.like(id, groupData.id, likes)();
                }}
              >
                {likes.length}
                <ThumbsUp />
                <Ink
                  style={{
                    color: liked
                      ? theme.extendedPalette.bluegrey["800"]
                      : theme.extendedPalette.red.a700,
                  }}
                />
              </button>
            </div>
          </div>
        </div>
        <style jsx>{`
          .post {
            cursor: pointer;
            display: flex;
            margin-bottom: ${theme.spacings["10"]};
            position: relative;
            text-align: left;
            width: 100%;
            &.no-modal {
              cursor: initial;
            }
          }

          .content {
            display: inline-block;
            flex: 1;
          }
          h3 {
            font-family: ${theme.fonts["sans-serif"]};
            font-size: 1.25rem;
            font-weight: ${theme.fontWeights.bold};
            width: 100%;
          }
          p {
            color: rgba(0, 0, 0, 0.7);
            font-size: ${theme.fontSizes.base};
            margin-top: ${theme.spacings["2"]};
            margin-bottom: ${theme.spacings["4"]};
            & span {
              margin: 0 ${theme.spacings["2"]};
            }
            & b {
              font-weight: ${theme.fontWeights.semibold};
            }
          }
          .likes {
            display: flex;
            justify-content: flex-end;
            margin-top: ${theme.spacings["4"]};
            & button {
              align-items: center;
              background: none;
              border: none;
              border-radius: 20px;
              color: ${liked
                ? theme.extendedPalette.blue.a700
                : theme.extendedPalette.bluegrey["800"]};
              display: flex;
              font-size: 1.25rem;
              position: relative;
              & :global(svg) {
                font-size: 1rem;
                margin-left: 5px;
              }
            }
          }
        `}</style>
      </>
    );
  }
);

interface PostProps {
  author: string;
  time: Date;
  content: string;
  likes: string[];
  id: string;
  modal: boolean;
}

namespace Post {
  const Base = withCSS<Omit<PostProps, "content"> & { desc: string }>(
    ({ author, time, theme, likes, id, modal, desc, children }) => {
      const [, profileCache] = useProfileCache();
      const [data, setData] = useState();
      const [pfp, setPfp] = useState("/default.png");
      const [, { get }] = useMediaCache();

      useSubscription(() => {
        return profileCache.get(author).subscribe((val) => setData(val.data()));
      });

      useEffect(() => {
        get(
          data?.pfp ? `pfp/${data.pfp}` : `pfp/default.png`
        ).subscribe((res) => setPfp(res));
      }, [data]);

      return (
        <_Post
          title={data?.name}
          desc={desc}
          time={moment(time).fromNow()}
          avatar={<ImgAvatar alt={"avatar"} src={pfp} />}
          id={id}
          likes={likes}
          liked={likes.includes(AuthProvider.user()?.uid || "")}
          modal={modal}
        >
          {children}
        </_Post>
      );
    }
  );
  export const Text = withCSS<PostProps>((props) => (
    <Base {...props} desc={"posted a new message"}>
      <p>{props.content}</p>
      <style jsx>{`
        p {
          font-size: ${props.theme.fontSizes.lg};
          line-height: 1.4;
        }
      `}</style>
    </Base>
  ));

  export const Announcement = withCSS<PostProps>((props) => (
    <Base {...props} desc={"made an announcement"}>
      <p>{props.content}</p>
      <style jsx>{`
        p {
          font-size: ${props.theme.fontSizes["2xl"]};
          font-weight: ${props.theme.fontWeights.medium};
          line-height: 1.4;
        }
      `}</style>
    </Base>
  ));

  export const Image = withCSS<PostProps & { caption?: string }>((props) => {
    const [url, setUrl] = useState("/default.png");
    const [, { get }] = useMediaCache();

    useSubscription(() => get(props.content).subscribe((url) => setUrl(url)));

    return (
      <Base {...props} desc={"posted an image"}>
        <img alt={props.caption} src={url} />
        <p>{props.caption}</p>
        <style jsx>{`
          img {
            border: 2px solid ${props.theme.colors.muted};
            border-radius: ${props.theme.radius};
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.02),
              0 2px 1px rgba(0, 0, 0, 0.012), 0 1px 1px rgba(0, 0, 0, 0.014);
            margin-bottom: ${props.theme.spacings["4"]};
            width: 100%;
          }
          p {
            border-left: 4px solid #666;
            color: #888;
            font-family: ${props.theme.fonts.serif};
            font-size: ${props.theme.fontSizes.xl};
            font-weight: ${props.theme.fontWeights.medium};
            font-style: italic;
            line-height: 1.4;
            padding: ${props.theme.spacings["3"]};
          }
        `}</style>
      </Base>
    );
  });

  export const Video = withCSS<PostProps & { caption?: string }>((props) => {
    const [url, setUrl] = useState();
    const [, { get }] = useMediaCache();

    useSubscription(() => get(props.content).subscribe((url) => setUrl(url)));

    return (
      <Base {...props} desc={"posted a video"}>
        {url && (
          <div
            onClick={(e) => {
              props.modal && e.stopPropagation();
            }}
          >
            <video src={url} controls autoPlay={!props.modal} loop />
          </div>
        )}
        <p>{props.caption}</p>
        <style jsx>{`
          div {
            border: 2px solid ${props.theme.colors.muted};
            border-radius: ${props.theme.radius};
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.02),
              0 2px 1px rgba(0, 0, 0, 0.012), 0 1px 1px rgba(0, 0, 0, 0.014);
            margin-bottom: ${props.theme.spacings["4"]};
            overflow: hidden;
          }
          video {
            background: black;
            display: block;
            max-height: 30rem;
            width: 100%;
          }
          p {
            border-left: 4px solid #666;
            color: #888;
            font-family: ${props.theme.fonts.serif};
            font-size: ${props.theme.fontSizes.xl};
            font-weight: ${props.theme.fontWeights.medium};
            font-style: italic;
            line-height: 1.4;
            padding: ${props.theme.spacings["3"]};
          }
        `}</style>
      </Base>
    );
  });
}

export default Post;
