import withCSS from "structure/withCSS";
import { Button, Input } from "components/atoms";
import { css } from "@emotion/core";
import ModalDialog, { ModalFooter } from "@atlaskit/modal-dialog";
import React, { useRef, useState } from "react";
import withTheme from "structure/withTheme";
import { of } from "rxjs";
import { catchError } from "rxjs/operators";
import Notifications from "store/Notifications";
import {
  AppearanceType,
  KeyboardOrMouseEvent,
} from "@atlaskit/modal-dialog/types";
import { FirebaseError } from "firebase/app";
import isError from "util/lambda/isError";
import Groups from "ctrl/Groups";
import AuthProvider from "ctrl/Auth";

const Body = withTheme(
  ({ spacings, fonts, fontWeights, fontSizes }) => css`
    padding: ${spacings["12"]} ${spacings["8"]} ${spacings["6"]}
      ${spacings["8"]} !important;
    text-align: center;
    h1 {
      font-family: ${fonts.serif};
      font-weight: ${fontWeights.bold};
      font-size: ${fontSizes["3xl"]};
      margin-bottom: ${spacings["12"]};
    }
  `
)("div");

const Footer = (create: () => void) =>
  withCSS<{
    appearance?: AppearanceType;
    onClose: (e: KeyboardOrMouseEvent) => void;
    showKeyline?: boolean;
  }>(({ onClose, theme }) => {
    const [disabled, setDisabled] = useState(false);
    return (
      <ModalFooter showKeyline={true}>
        <div>
          <Button color={"error"} ghost onClick={onClose}>
            Close
          </Button>
          <Button
            color={"primary"}
            disabled={disabled}
            onClick={() => {
              setDisabled(true);
              create();
            }}
          >
            Create
          </Button>
        </div>
        <style jsx>{`
          div {
            display: flex;
            justify-content: flex-end;
            width: 100%;
            & :global(button:first-of-type) {
              margin-right: ${theme.spacings["6"]};
            }
          }
        `}</style>
      </ModalFooter>
    );
  });

const NewGroup = withCSS<{
  close: () => void;
  exit: (group: { name: string; id: string }) => void;
}>(({ close, exit, theme }) => {
  const contentRef = useRef<HTMLInputElement>();

  const create = () => {
    Groups.create(
      contentRef.current?.value as string,
      AuthProvider.user()?.uid as string
    )
      .pipe(catchError((err) => of(err as Error)))
      .subscribe((value) => {
        close();
        let err = value as FirebaseError;
        if (isError(err)) {
          Notifications.push({
            type: "error",
            title: "Error: ",
            desc: "an error occurred while creating a new group",
          });
          console.log(err);
        } else {
          Notifications.push({
            type: "success",
            title: "Group created!",
          });
          exit({
            name: contentRef.current?.value as string,
            id: (value as firebase.firestore.DocumentReference).id,
          });
        }
      });
  };

  return (
    <ModalDialog
      onClose={close}
      components={{ Body, Footer: Footer(create) }}
      css={css`
        border-radius: ${theme.radius};
      `}
    >
      <h1>New Group</h1>
      <Input color={"primary"} placeholder={"Group name"} ref={contentRef} />
    </ModalDialog>
  );
});

export default NewGroup;
