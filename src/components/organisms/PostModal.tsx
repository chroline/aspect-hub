import withCSS from "structure/withCSS";
import { Box, Button } from "components/atoms";
import { css } from "@emotion/core";
import { ModalFooter } from "@atlaskit/modal-dialog";
import React, { useEffect, useState } from "react";
import withTheme from "structure/withTheme";
import { AppearanceType } from "@atlaskit/modal-dialog/types";
import { useEffectOnce, useToggle, useWindowSize } from "react-use";
import CreatableSelect from "react-select/creatable";
import useSubscription from "util/hooks/useSubscription";
import Firebase from "store/Firebase";
import { useGroupData } from "store/GroupData";
import { doc } from "rxfire/firestore";
import { colors } from "util/theme/colors";
import { radius } from "util/theme/constants";
import Capsules from "ctrl/Capsules";
import { catchError, map } from "rxjs/operators";
import { from, of } from "rxjs";
import Notifications from "store/Notifications";
import { ValueType } from "react-select";
import Post, { PostCapsuleData } from "util/interfaces/Post";
import { FirebaseError } from "firebase/app";
import isError from "util/lambda/isError";
import { Dialog } from "@material-ui/core";
import PostsRenderer from "components/organisms/PostsRenderer";
import remToPx from "util/lambda/remToPx";
import { default as CloseIcon } from "@atlaskit/icon/glyph/cross";
import AuthProvider from "ctrl/Auth";
import { Trash2 } from "react-feather";
import Posts from "ctrl/Posts";
import { useHistory } from "react-router";

const TopBar = withTheme(
  ({ fontSizes, fontWeights, colors }) => css`
    align-items: center;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.02), 0 2px 1px rgba(0, 0, 0, 0.012),
      0 1px 1px rgba(0, 0, 0, 0.014);
    display: flex;
    flex-direction: row;
    font-size: ${fontSizes["xl"]};
    font-weight: ${fontWeights.bold};
    height: 56px;
    justify-content: flex-start;
    padding: 0 12px;
    text-align: center;
    width: 100%;
    div {
      flex: 1;
    }
    button {
      background: none;
      border: none;
      color: ${colors.primary};
      font-size: ${fontSizes["xl"]};
      font-weight: ${fontWeights.bold};
      padding: 0;
    }
  `
)("div");

const Body = withTheme(
  ({ spacings, fonts, fontWeights, fontSizes }) => css`
    flex: 1;
    height: initial;
    overflow: scroll;
    padding: ${spacings["8"]};
    text-align: center;
    h1 {
      font-family: ${fonts.serif};
      font-weight: ${fontWeights.bold};
      font-size: ${fontSizes["3xl"]};
      margin-bottom: ${spacings["12"]};
    }
  `
)("div");

const Footer: React.FC<{
  appearance?: AppearanceType;
  showKeyline?: boolean;
}> = ({ children }) => {
  return (
    <ModalFooter showKeyline={true}>
      <div>{children}</div>
      <style jsx>{`
        div {
          display: flex;
          justify-content: flex-end;
          width: 100%;
          & :global(button) {
            margin-left: 24px;
          }
        }
      `}</style>
    </ModalFooter>
  );
};

const customStyles = {
  container: (provided: React.CSSProperties) => ({
    ...provided,
    flex: 1,
  }),
  control: (provided: React.CSSProperties) => ({
    ...provided,
    fontSize: "1rem",
  }),
  menu: (provided: React.CSSProperties) => ({
    ...provided,
    background: "white",
    textAlign: "left" as "left",
    zIndex: 999,
  }),
};

const createCapsule = (label: string, groupID: string) => {
  const creator = Capsules.create(label, groupID).pipe(
    catchError((err) => of(err as Error))
  );
  creator.subscribe((value) => {
    let err = value as FirebaseError;
    if (isError(err)) {
      Notifications.push({
        type: "warning",
        title: "Error: ",
        desc: "an error occurred while creating a new capsule",
      });
    }
  });
  return creator;
};

const CapsuleSelector: React.FC<{
  postID: string;
}> = ({ postID }) => {
  const [loading, setLoading] = useToggle(false);
  const [options, setOptions] = useState<
    {
      label: string;
      value: string;
    }[]
  >([]);
  const [value, setValue] = useState<PostCapsuleData | undefined>(undefined);
  const [groupData] = useGroupData();

  useEffectOnce(() => setLoading(true));

  const db = Firebase.app.firestore();
  const postDoc = db.doc(`groups/${groupData.id}/posts/${postID}`);

  useEffect(() => {
    const listener = doc(postDoc).subscribe((value) => {
      setLoading(false);
      const capsule = value.data()?.capsule;
      setValue(
        capsule === ""
          ? undefined
          : options.filter((v) => v.value === capsule)[0]
      );
    });
    return () => listener.unsubscribe();
  }, [options]);

  useSubscription(() => {
    const doc$ = doc(db.doc(`groups/${groupData.id}`));
    return doc$.subscribe((snapshot) => {
      const options: PostCapsuleData[] = [];
      Object.entries(snapshot.data()?.capsules || {}).forEach(([name, id]) => {
        options.push({ label: name, value: id as string });
      });
      setOptions(options);
    });
  });

  const handleChange = (newValue: ValueType<PostCapsuleData> | null) => {
    setLoading(true);
    from(
      postDoc.update({
        capsule: newValue ? (newValue as PostCapsuleData).value : null,
      })
    )
      .pipe(catchError((err) => of(err as Error)))
      .subscribe((res) => {
        let err = res as FirebaseError;
        if (isError(err))
          return Notifications.push({
            type: "error",
            title: "Error adding to capsule",
            desc: "an error occurred while updating this post",
          });
        if (newValue === null)
          Capsules.removePost(value?.value as string, postID, groupData.id)
            .pipe(catchError((err) => of(err as Error)))
            .subscribe((res) => {
              setLoading(false);
              let err = res as FirebaseError;
              if (isError(err))
                return Notifications.push({
                  type: "error",
                  title: "Error removing from capsule",
                  desc:
                    "an error occurred while removing this post from a capsule",
                });
            });
        else
          Capsules.addPost(
            (newValue as PostCapsuleData).value as string,
            postID,
            groupData.id
          )
            .pipe(catchError((err) => of(err as Error)))
            .subscribe((res) => {
              setLoading(false);
              let err = res as FirebaseError;
              if (isError(err))
                return Notifications.push({
                  type: "error",
                  title: "Error adding to capsule",
                  desc: "an error occurred while adding this post to a capsule",
                });
            });
      });
  };

  const handleCreate = (label: string) => {
    setLoading(true);
    createCapsule(label, groupData.id).subscribe((value) => {
      let err = value as FirebaseError;
      if (err.name !== "FirebaseError") {
        handleChange({
          label,
          value: (value as firebase.firestore.DocumentReference).id,
        });
      }
    });
  };

  return (
    <CreatableSelect
      styles={customStyles}
      isClearable
      isDisabled={loading}
      isLoading={loading}
      onChange={handleChange}
      onCreateOption={handleCreate}
      options={options}
      value={value}
      placeholder={"Add to a capsule"}
      noOptionsMessage={() => "No capsules found"}
      menuPlacement={"top"}
      theme={(theme: any) => ({
        ...theme,
        borderRadius: radius,
        colors: {
          ...theme.colors,
          primary: colors.primary,
        },
      })}
    />
  );
};

const PostModal = withCSS<{
  open: boolean;
  close: () => void;
  postID?: string;
}>(({ close, theme, children, postID, open }) => {
  const [groupData] = useGroupData();
  const [post, setPost] = useState<Post>();
  const [loading, setLoading] = useToggle(false);
  const history = useHistory();

  const db = Firebase.app.firestore();
  useEffect(() => {
    setLoading(false);
    if (postID) {
      const listener = doc(db.doc(`groups/${groupData.id}/posts/${postID}`))
        .pipe(
          map((val) => ({
            id: val.id,
            exists: val.exists,
            ...val.data(),
          }))
        )
        .subscribe((value) => {
          if (!value.exists) {
            Notifications.push({
              type: "error",
              title: "Error retrieving post: ",
              desc: "we couldn't find this post.",
            });
            history.replace("/");
          }
          setPost(value as any);
        });
      return () => listener.unsubscribe();
    }
  }, [postID]);

  const remove = () => {
    close();
    if (post) {
      setLoading(true);
      Posts.remove(post, groupData.id).subscribe((value) => {
        let err = value as FirebaseError;
        if (isError(err)) {
          Notifications.push({
            type: "warning",
            title: "Error: ",
            desc: "an error occurred while deleting the post",
          });
        } else {
          Notifications.push({
            type: "success",
            title: "Post deleted",
            desc: "",
          });
        }
      });
    }
  };

  const fullScreen = useWindowSize().width < remToPx(50);
  return (
    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={close}
      className={"DIALOG"}
    >
      {fullScreen && (
        <TopBar>
          <Box
            css={css`
              text-align: left;
            `}
          >
            <button onClick={close}>
              <CloseIcon label={"close"} />
            </button>
          </Box>
          <div>Post</div>
          <div />
        </TopBar>
      )}
      <Body>
        <PostsRenderer posts={post ? [post] : []} modal={false} />
      </Body>
      <Footer>
        {postID && <CapsuleSelector postID={postID} />}
        {post?.author === AuthProvider.user()?.uid && (
          <Button onClick={remove} color={"error"} disabled={loading}>
            <Trash2 />
          </Button>
        )}
      </Footer>
      <style jsx>{`
        div {
          border-radius: ${radius};
          display: flex;
          flex-direction: column;
          overflow: scroll;
        }
      `}</style>
    </Dialog>
  );
});

export default PostModal;
