import withCSS from "structure/withCSS";
import { Button, Input } from "components/atoms";
import { css } from "@emotion/core";
import ModalDialog, { ModalFooter } from "@atlaskit/modal-dialog";
import React, { useRef, useState } from "react";
import withTheme from "structure/withTheme";
import { useGroupData } from "store/GroupData";
import { of } from "rxjs";
import { catchError } from "rxjs/operators";
import Notifications from "store/Notifications";
import {
  AppearanceType,
  KeyboardOrMouseEvent,
} from "@atlaskit/modal-dialog/types";
import Capsules from "ctrl/Capsules";
import { FirebaseError } from "firebase/app";
import isError from "util/lambda/isError";

const Body = withTheme(
  ({ spacings, fonts, fontWeights, fontSizes }) => css`
    padding: ${spacings["12"]} ${spacings["8"]} ${spacings["6"]}
      ${spacings["8"]} !important;
    text-align: center;
    h1 {
      font-family: ${fonts.serif};
      font-weight: ${fontWeights.bold};
      font-size: ${fontSizes["3xl"]};
      margin-bottom: ${spacings["12"]};
    }
  `
)("div");

const Footer = (
  create: () => void,
  [disabled, setDisabled]: [boolean, (v: boolean) => void]
) =>
  withCSS<{
    appearance?: AppearanceType;
    onClose: (e: KeyboardOrMouseEvent) => void;
    showKeyline?: boolean;
  }>(({ onClose, theme }) => (
    <ModalFooter showKeyline={true}>
      <div>
        <Button color={"error"} ghost onClick={onClose}>
          Close
        </Button>
        <Button
          color={"primary"}
          disabled={disabled}
          onClick={() => {
            create();
            setDisabled(true);
          }}
        >
          Create
        </Button>
      </div>
      <style jsx>{`
        div {
          display: flex;
          justify-content: flex-end;
          width: 100%;
          & :global(button:first-of-type) {
            margin-right: ${theme.spacings["6"]};
          }
        }
      `}</style>
    </ModalFooter>
  ));

const NewCapsule = withCSS<{ close: () => void }>(({ close, theme }) => {
  const contentRef = useRef<HTMLInputElement>();
  const disabled = useState(false);

  const [groupData] = useGroupData();
  const create = () => {
    Capsules.create(contentRef.current?.value || "", groupData.id)
      .pipe(catchError((err) => of(err as Error)))
      .subscribe((value) => {
        close();
        let err = value as FirebaseError;
        if (isError(err)) {
          Notifications.push({
            type: "warning",
            title: "Error: ",
            desc: "an error occurred while creating a new capsule",
          });
        } else {
          Notifications.push({
            type: "success",
            title: "Capsule created!",
            desc: "",
          });
        }
      });
  };

  return (
    <ModalDialog
      onClose={close}
      components={{ Body, Footer: Footer(create, disabled) }}
      css={css`
        border-radius: ${theme.radius};
      `}
    >
      <h1>New Capsule</h1>
      <Input color={"primary"} placeholder={"Capsule name"} ref={contentRef} />
    </ModalDialog>
  );
});

export default NewCapsule;
