import React from "react";
import withCSS from "structure/withCSS";
import { Lock } from "react-feather";

const CapsuleLocked = withCSS<{ date: Date }>(({ theme, date }) => {
  return (
    <>
      <div>
        <Lock />
        <h2>This capsule is locked</h2>
        <p>
          Opening date: <b>{date.toLocaleDateString()}</b>
        </p>
      </div>
      <style jsx>{`
        div {
          align-items: center;
          color: ${theme.extendedPalette.bluegrey["800"]};
          display: flex;
          flex-direction: column;
          height: 100%;
          justify-content: center;
          & :global(svg) {
            height: ${theme.spacings["32"]};
            width: ${theme.spacings["32"]};
          }
        }
        h2 {
          font-family: ${theme.fonts.serif};
          font-size: ${theme.fontSizes["3xl"]};
          margin-top: ${theme.spacings["6"]};
        }
        p {
          font-family: ${theme.fonts["sans-serif"]};
          font-size: ${theme.fontSizes["xl"]};
          margin-top: ${theme.spacings["6"]};
          & b {
            font-weight: ${theme.fontWeights.bold};
          }
        }
      `}</style>
    </>
  );
});

export default CapsuleLocked;
