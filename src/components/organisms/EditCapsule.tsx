import withCSS from "structure/withCSS";
import { Button, Input } from "components/atoms";
import { ClassNames, css } from "@emotion/core";
import ModalDialog, { ModalFooter } from "@atlaskit/modal-dialog";
import React, { useState } from "react";
import withTheme from "structure/withTheme";
import { useGroupData } from "store/GroupData";
import { Observable, of } from "rxjs";
import { catchError, map } from "rxjs/operators";
import {
  AppearanceType,
  KeyboardOrMouseEvent,
} from "@atlaskit/modal-dialog/types";
import useSubscription from "util/hooks/useSubscription";
import Firebase from "store/Firebase";
import { doc } from "rxfire/firestore";
import Post from "util/interfaces/Post";
import Capsule from "util/interfaces/Capsule";
import { DatePicker } from "@atlaskit/datetime-picker";
import Capsules from "ctrl/Capsules";
import isError from "util/lambda/isError";
import Notifications from "store/Notifications";

const Body = withTheme(
  ({ spacings, fonts, fontWeights, fontSizes }) => css`
    padding: ${spacings["12"]} ${spacings["8"]} ${spacings["6"]}
      ${spacings["8"]} !important;
    text-align: center;
    h1 {
      font-family: ${fonts.serif};
      font-weight: ${fontWeights.bold};
      font-size: ${fontSizes["3xl"]};
      margin-bottom: ${spacings["12"]};
    }
  `
)("div");

const Item = withTheme(
  ({ spacings }) => css`
    margin-bottom: ${spacings["5"]};
    margin-top: ${spacings["12"]};
    text-align: left;
  `
)("div");

const Label = withTheme(
  ({ fontSizes, fontWeights, spacings }) => css`
    color: rgba(0, 0, 0, 0.8);
    font-size: ${fontSizes.lg};
    font-weight: ${fontWeights.bold};
    margin-bottom: ${spacings["5"]};
  `
)("p");

const Line = withTheme(
  ({ spacings, radius }) =>
    css`
      border: 1px solid #dfdfdf;
      border-radius: ${radius};
      margin-top: ${spacings["12"]};
    `
)("hr");

const Footer = (create: () => void) =>
  withCSS<{
    appearance?: AppearanceType;
    onClose: (e: KeyboardOrMouseEvent) => void;
    showKeyline?: boolean;
  }>(({ onClose, theme }) => {
    const [disabled, setDisabled] = useState(false);
    return (
      <ModalFooter showKeyline={true}>
        <div>
          <Button color={"error"} ghost onClick={onClose}>
            Close
          </Button>
          <Button
            color={"primary"}
            disabled={disabled}
            onClick={() => {
              setDisabled(true);
              create();
            }}
          >
            Update
          </Button>
        </div>
        <style jsx>{`
          div {
            display: flex;
            justify-content: flex-end;
            width: 100%;
            & :global(button:first-of-type) {
              margin-right: ${theme.spacings["6"]};
            }
          }
        `}</style>
      </ModalFooter>
    );
  });

const EditCapsule = withCSS<{
  close: () => void;
  exit: () => Observable<void>;
  id: string;
}>(({ close, exit, id, theme }) => {
  const [groupData] = useGroupData();
  const [name, setName] = useState("");
  const [lock, setLock] = useState<string>();

  useSubscription(() =>
    (doc(
      Firebase.app
        .firestore()
        .doc(`groups/${groupData?.id as string}/capsules/${id}`)
    ) as Observable<firebase.firestore.DocumentSnapshot<Post>>)
      .pipe(map((v) => ({ id: v.id, ...v.data() } as Capsule)))
      .subscribe((v) => {
        setName(v.name);
        setLock(v.lock?.toDate().toISOString());
      })
  );

  const update = () => {
    let date: Date = (null as unknown) as Date;
    if (lock) {
      date = new Date();
      date.setFullYear(lock.split("-")[0] as any);
      date.setMonth(((lock.split("-")[1] as unknown) as number) - 1);
      date.setUTCDate(lock.split("-")[2] as any);
      date.setHours(0, 0, 0, 0);
      console.log(date);
    }
    Capsules.update(id, { name, lock: date }, groupData?.id as string)
      .pipe(catchError((err) => of(err as Error)))
      .subscribe((res) => {
        let err = res as Error;
        if (!isError(err))
          Notifications.push({
            type: "success",
            title: "Capsule updated!",
          });
        else
          Notifications.push({
            type: "error",
            title: "Error updating capsule",
            desc: "please try again later",
          });
      });
  };

  const remove = () => {
    exit().subscribe(() =>
      Capsules.remove(id, groupData?.id as string)
        .pipe(catchError((err) => of(err as Error)))
        .subscribe((res) => {
          let err = res as Error;
          if (!isError(err))
            Notifications.push({
              type: "success",
              title: "Capsule deleted",
            });
          else
            Notifications.push({
              type: "error",
              title: "Error deleting capsule",
              desc: "please try again later",
            });
        })
    );
  };

  return (
    <ModalDialog
      onClose={close}
      components={{ Body, Footer: Footer(update) }}
      css={css`
        border-radius: ${theme.radius};
      `}
      autoFocus={false}
    >
      <h1>Edit Capsule</h1>
      <Item>
        <Label>Name</Label>
        <Input
          color={"primary"}
          placeholder={"Group name"}
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </Item>
      <Item>
        <Label>Time Capsule Lock</Label>
        <ClassNames>
          {({ css }) => (
            <DatePicker
              value={lock}
              onChange={(e) => {
                console.log(e);
                setLock(e);
              }}
              selectProps={{
                placeholder: "Choose date to unlock capsule",
              }}
              innerProps={{
                className: css`
                  > div > div {
                    border: none !important;
                    background: transparent !important;
                    height: 3.5rem;
                    font-family: ${theme.fonts["sans-serif"]};
                    font-size: 1.125rem;
                  }
                `,
              }}
              autoFocus={false}
              defaultIsOpen={false}
              defaultValue={undefined}
              disabled={undefined}
              icon={undefined}
              id={undefined}
              locale={undefined}
              name={undefined}
              onBlur={undefined}
              onFocus={undefined}
            />
          )}
        </ClassNames>
      </Item>
      <Line />
      <Item>
        <Button color={"error"} ghost block onClick={remove}>
          Delete
        </Button>
      </Item>
    </ModalDialog>
  );
});

export default EditCapsule;
