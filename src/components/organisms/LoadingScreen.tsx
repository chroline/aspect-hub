import React from "react";
import styled from "@emotion/styled";
import { GridLoader } from "react-spinners";
import { colors } from "util/theme/colors";
import { css } from "@emotion/core";
import { motion } from "framer-motion";
import { transition } from "util/theme/constants";
import withTheme from "structure/withTheme";

const Wrapper = withTheme(
  () => css`
    align-items: center;
    background: rgba(255, 255, 255, 0.9);
    backdrop-filter: saturate(180%) blur(20px);
    display: flex;
    height: 100%;
    left: 0;
    justify-content: center;
    position: fixed;
    top: 0;
    width: 100%;
  `
)(motion.div);

const Content = styled.div`
  align-items: center;
  display: inline-flex;
  flex-direction: column;
  max-width: 20rem;
  padding: 0 2rem;
`;

const pageVariants = {
  initial: {
    opacity: 0,
    scale: 1.2,
  },
  in: {
    opacity: 1,
    scale: 1,
  },
  out: {
    opacity: 0,
    scale: 1.5,
  },
};

const LoadingScreen = () => {
  return (
    <Wrapper
      key={"loading"}
      initial="initial"
      animate="in"
      exit="out"
      variants={pageVariants}
      transition={transition}
    >
      <Content>
        <GridLoader
          css={css`
            display: block;
          `}
          size={20}
          color={colors.primary}
          loading={true}
        />
      </Content>
    </Wrapper>
  );
};

export default LoadingScreen;
