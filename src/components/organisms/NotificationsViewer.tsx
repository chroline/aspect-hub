import React, { useState } from "react";
import Notifications from "store/Notifications";
import { css } from "@emotion/core";
import { Alert, Box } from "components/atoms";
import { AnimatePresence } from "framer-motion";
import { transition } from "util/theme/constants";
import { useMount } from "react-use";

const NotificationsViewer = () => {
  const [notifs, setNotifs] = useState<any[]>([]);

  useMount(() =>
    Notifications.Collection.subscribe((value) => setNotifs(value))
  );

  return (
    <Box
      css={css`
        left: 0;
        padding: 0 1rem;
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 999;
        div {
          margin: 1rem 0;
        }
      `}
      role={"alertdialog"}
    >
      <AnimatePresence initial={false}>
        {[...notifs].reverse().map((content) => {
          return (
            <Alert
              type={content.type}
              key={JSON.stringify(content)}
              positionTransition={transition}
              initial={{ opacity: 0, x: -50 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0, x: -50 }}
              transition={transition}
              onClick={() => Notifications.remove(content)}
            >
              <p>
                <b>{content.title}</b>
                {content.desc}
              </p>
            </Alert>
          );
        })}
      </AnimatePresence>
    </Box>
  );
};

export default NotificationsViewer;
