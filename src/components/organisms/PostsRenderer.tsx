import Post from "util/interfaces/Post";
import PostView from "components/molecules/Post";
import React from "react";
import iife from "util/iife";
import { motion } from "framer-motion";
import { transition } from "util/theme/constants";

const animationVariants = {
  hidden: { opacity: 0 },
  show: { opacity: 1 },
};

const PostsRenderer: React.FC<{ posts: Post[]; modal?: boolean }> = ({
  posts,
  modal,
}) => (
  <>
    {posts.map((value) => (
      <motion.div
        variants={animationVariants}
        positionTransition={transition}
        key={value.id}
      >
        {iife(() => {
          switch (value.type) {
            case "text":
              return (
                <PostView.Text
                  key={value.id}
                  id={value.id}
                  author={value.author}
                  time={value.date.toDate()}
                  content={value.content}
                  likes={value.likes}
                  modal={modal !== undefined ? modal : true}
                />
              );
            case "announcement":
              return (
                <PostView.Announcement
                  key={value.id}
                  id={value.id}
                  author={value.author}
                  time={value.date.toDate()}
                  content={value.content}
                  likes={value.likes}
                  modal={modal !== undefined ? modal : true}
                />
              );
            case "image":
              return (
                <PostView.Image
                  key={value.id}
                  id={value.id}
                  author={value.author}
                  time={value.date.toDate()}
                  content={value.content}
                  caption={value.caption}
                  likes={value.likes}
                  modal={modal !== undefined ? modal : true}
                />
              );
            case "video":
              return (
                <PostView.Video
                  key={value.id}
                  id={value.id}
                  author={value.author}
                  time={value.date.toDate()}
                  content={value.content}
                  caption={value.caption}
                  likes={value.likes}
                  modal={modal !== undefined ? modal : true}
                />
              );
          }
        })}
      </motion.div>
    ))}
  </>
);

export default PostsRenderer;
