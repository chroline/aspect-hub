import { SVGInjector } from "@tanem/svg-injector";
import React, { useLayoutEffect, useRef } from "react";

const SVG: React.FC<{ src: string } & React.HTMLAttributes<HTMLDivElement>> = (
  props
) => {
  const ref = useRef(null);

  useLayoutEffect(() => {
    SVGInjector(ref.current as any);
  });

  return (
    <div {...props}>
      <svg ref={ref} data-src={props.src} />
    </div>
  );
};

export default SVG;
