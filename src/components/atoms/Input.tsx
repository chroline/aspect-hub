import React from "react";
import { colors } from "util/theme/colors";
import get from "util/lambda/get";
import Color from "color";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";

export interface InputProps {
  color: keyof typeof colors;
  disabled?: boolean;
}

const Input = withTheme<InputProps>(
  ({ colors, radius, fonts }, props) => css`
    background: ${!props.disabled ? "#fafbfc" : "#e3e3e3"};
    border: 2px solid
      ${props.color !== "primary" ? get(props.color, colors) : "#DFDFDF"};
    border-radius: ${radius};
    box-shadow: none !important;
    color: ${!props.disabled ? "black" : "#999"};
    height: 3.5rem;
    font-family: ${fonts["sans-serif"]};
    font-size: 1.125rem;
    font-weight: 400;
    padding: 0 1.4rem;
    transition: all 0.3s;
    width: 100%;

    &:focus {
      background: white;
      border-color: ${get(props.color, colors).toString()};
      box-shadow: 0 0 0 4px
        ${Color(get(props.color, colors)).alpha(0.25).toString()} !important;
      z-index: 1;
    }
  `
)("input");

export default Input;
