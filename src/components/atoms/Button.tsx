import React, { MouseEventHandler } from "react";
import { colors } from "util/theme/colors";
import Ink from "react-ink";
import iife from "util/iife";
import * as constants from "util/theme/constants";
import get from "util/lambda/get";
import Color from "color";
import { css } from "@emotion/core";
import withTheme from "structure/withTheme";

export type ButtonSizes = "mini" | "small" | "medium" | "large" | "xlarge";

export interface ButtonProps {
  block?: boolean;
  color: keyof typeof colors;
  disabled?: boolean;
  ghost?: boolean;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  size?: ButtonSizes;
  type?: "submit" | "reset" | "button";
}

const height = (size: ButtonSizes) =>
  iife(() => {
    switch (size) {
      case "mini":
        return 1.75;
      case "small":
        return 2.25;
      case "medium":
        return 2.75;
      case "large":
        return 3;
      case "xlarge":
        return 3.5;
    }
  });

const padding = (size: ButtonSizes) =>
  iife(() => {
    switch (size) {
      case "mini":
      case "large":
      case "xlarge":
        return 1.9;
      case "small":
        return 1.275;
      case "medium":
        return 1.4;
    }
  });

const fontSize = (size: ButtonSizes) =>
  iife(() => {
    switch (size) {
      case "mini":
        return 0.875;
      case "small":
      case "medium":
        return 1;
      case "large":
      case "xlarge":
        return 1.125;
    }
  });

const Button = withTheme<ButtonProps>(
  (theme, { ghost, color, disabled, size, block }) => css`
    align-items: center;
    background: ${ghost
      ? iife(() => {
          if (disabled) return "#c7c7c7";
          return "transparent";
        })
      : iife(() => {
          if (disabled)
            return Color(get(color, colors))
              .lighten(0.1)
              .desaturate(0.5)
              .toString();
          return get(color, colors);
        })};
    border: 2px solid ${get(color, colors)};
    border-radius: ${constants.radius};
    color: ${ghost
      ? get(color, colors)
      : iife(() => {
          if (Color(get(color, colors)).isDark()) return "white";
          return "black";
        })};
    display: flex;
    height: ${height(size || "medium")}rem;
    ${disabled ? `filter: brightness(1.5) grayscale(0.5);` : ""};
    font-family: ${theme.fonts["sans-serif"]};
    font-size: ${fontSize(size || "medium")}rem;
    font-weight: 500;
    justify-content: center;
    letter-spacing: 1.25px;
    overflow: hidden;
    padding: 0 ${padding(size || "medium")}rem;
    position: relative;
    text-align: center;
    text-transform: uppercase;
    transition: all 0.3s;
    width: ${block ? "100%" : "initial"};
    &:focus,
    &:active {
      background: ${ghost
        ? `${Color(get(color, colors)).alpha(0.1).toString()};`
        : ""};
    }
  `
)(
  (props) => (
    <button {...props}>
      <Ink
        opacity={0.5}
        style={{
          color: !props.disabled
            ? props.ghost
              ? props.color
              : iife(() => {
                  if (Color(get(props.color, colors)).isDark()) return "white";
                  return "black";
                })
            : "transparent",
        }}
      />
      {props.children}
    </button>
  ),
  ["block", "ghost", "size"]
);

export default Button;
