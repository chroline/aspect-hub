import React from "react";
import { colors } from "util/theme/colors";
import get from "util/lambda/get";
import Color from "color";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import TextareaAutosize from "react-autosize-textarea";

export interface TextAreaProps {
  color: keyof typeof colors;
}

const TextArea = withTheme<TextAreaProps>(
  ({ colors, radius, fonts }, props) => css`
    background: #fafbfc;
    border: 2px solid
      ${props.color !== "primary" ? get(props.color, colors) : "#DFDFDF"};
    border-radius: ${radius};
    box-shadow: none !important;
    color: black;
    font-family: ${fonts["sans-serif"]};
    font-size: 1.125rem;
    font-weight: 400;
    padding: 1rem 1.4rem;
    transition: all 0.3s;
    width: 100%;

    &:focus,
    &:active {
      background: white;
      border-color: ${get(props.color, colors).toString()};
      box-shadow: 0 0 0 4px
        ${Color(get(props.color, colors)).alpha(0.25).toString()} !important;
      z-index: 1;
    }
  `
)(TextareaAutosize);

export default TextArea;
