import React from "react";
import iife from "util/iife";
import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import { motion } from "framer-motion";

const Alert = withTheme<{ type: "success" | "error" | "warning" | "info" }>(
  ({ extendedPalette, spacings, fontSizes, fontWeights }, props) => css`
    background-color: ${iife(() => {
      switch (props.type) {
        case "success":
          return extendedPalette.green["50"];
        case "error":
          return extendedPalette.deeporange["50"];
        case "warning":
          return extendedPalette.lime["50"];
        case "info":
          return extendedPalette.deeppurple["50"];
      }
    })};
    border-left: 5px solid
      ${iife(() => {
        switch (props.type) {
          case "success":
            return extendedPalette.green["600"];
          case "error":
            return extendedPalette.deeporange["600"];
          case "warning":
            return extendedPalette.lime["600"];
          case "info":
            return extendedPalette.deeppurple["600"];
        }
      })};
    box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);
    padding: ${spacings[4]} calc(1.4rem - 5px);
    width: 100%;
    p {
      color: black;
      margin: 0;
      font-size: ${fontSizes.lg};
      line-height: 1.4;
      b {
        font-weight: ${fontWeights.semibold};
      }
    }
  `
)(motion.div);

export default Alert;
