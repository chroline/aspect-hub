import { css } from "@emotion/core";
import withTheme from "structure/withTheme";

const Box = withTheme(() => css``)("div");

export default Box;
