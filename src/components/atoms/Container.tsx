import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import iife from "util/iife";

const Container = withTheme<{ size?: "sm" | "md" | "lg" }>(
  (_, { size }) => css`
    max-width: ${iife(() => {
      switch (size) {
        case "sm":
          return "40rem";
        case "lg":
          return "60rem";
        case "md":
        default:
          return "50rem";
      }
    })};
    width: 100%;
  `
)("div");

export default Container;
