import withTheme from "structure/withTheme";
import { css } from "@emotion/core";
import React, { useEffect, useState } from "react";
import { useMorph } from "react-morph";
import remToPx from "util/lambda/remToPx";
import { Flipped, Flipper } from "react-flip-toolkit";
import Ink from "react-ink";
import Color from "color";

const _FAB = withTheme<{ bottom: boolean; muted: boolean }>(
  ({ colors, extendedPalette }, { bottom, muted }) => css`
    align-items: center;
    background: ${muted ? extendedPalette.bluegrey["800"] : colors.primary};
    border: none;
    border-radius: 50%;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 1px 10px rgba(0, 0, 0, 0.12),
      0 4px 5px rgba(0, 0, 0, 0.14);
    ${bottom && "bottom: calc(56px + env(safe-area-inset-bottom) + 1rem)"};
    color: rgba(255, 255, 255, 0.87);
    cursor: pointer;
    display: flex;
    height: 4rem;
    justify-content: center;
    position: ${!bottom ? "absolute" : "fixed"};
    right: 1rem;
    transition: box-shadow 0.28s cubic-bezier(0.4, 0, 0.2, 1);
    width: 4rem;
    z-index: 5;
    &:active {
      box-shadow: 0 7px 8px -4px rgba(0, 0, 0, 0.2),
        0 12px 17px 2px rgba(0, 0, 0, 0.14), 0 5px 22px 4px rgba(0, 0, 0, 0.12);
    }
    svg {
      height: 24px;
      width: 24px;
    }
  `
)("button", ["bottom"]);

const ExtendedFAB = withTheme<{
  bottom: boolean;
  fixed: boolean;
  disabled?: boolean;
}>(
  (
    { colors, spacings, fontWeights, fontSizes },
    { bottom, fixed, disabled }
  ) => css`
    align-items: center;
    background: ${colors.primary};
    background: ${!disabled
      ? colors.primary
      : Color(colors.primary).lighten(0.1).desaturate(0.5).toString()};
    border: none;
    border-radius: 32px;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 1px 10px rgba(0, 0, 0, 0.12),
      0 4px 5px rgba(0, 0, 0, 0.14);
    bottom: ${!bottom
      ? "calc(56px + env(safe-area-inset-bottom) + 1rem)"
      : "calc(env(safe-area-inset-bottom) + 1rem)"};
    color: rgba(255, 255, 255, 0.87);
    display: flex;
    padding: calc(2rem - 12px);
    position: ${fixed ? "fixed" : "absolute"};
    right: 1rem;
    transition: box-shadow 0.28s cubic-bezier(0.4, 0, 0.2, 1);
    &:active:not(:disabled) {
      box-shadow: 0 7px 8px -4px rgba(0, 0, 0, 0.2),
        0 12px 17px 2px rgba(0, 0, 0, 0.14), 0 5px 22px 4px rgba(0, 0, 0, 0.12);
    }
    svg {
      height: 24px;
      width: 24px;
    }
    p {
      font-size: ${fontSizes.lg};
      font-weight: ${fontWeights.semibold};
      margin-left: calc(1.5rem - 12px);
      letter-spacing: 1.125px;
      text-transform: uppercase;
    }
  `
)("button", ["bottom", "fixed"]);

namespace FAB {
  export const Normal: React.FC<{
    onClick: () => void;
    scroll: number;
    muted?: boolean;
  }> = ({ onClick, scroll, children, muted }) => {
    const [b, sB] = useState(false);
    const morph = useMorph();

    useEffect(() => sB(scroll > remToPx(6) - 22), [scroll]);

    return (
      <Flipper flipKey={b} spring={"stiff"}>
        <Flipped flipId="square">
          <_FAB {...morph} bottom={b} onClick={onClick} muted={muted || false}>
            {children}
            <Ink />
          </_FAB>
        </Flipped>
      </Flipper>
    );
  };

  export const Extended: React.FC<{
    onClick: () => void;
    bottom: boolean;
    fixed: boolean;
    disabled?: boolean;
  }> = ({ children, bottom, fixed, onClick, disabled }) => (
    <ExtendedFAB
      onClick={onClick}
      bottom={bottom}
      fixed={fixed}
      disabled={disabled}
    >
      <Ink
        opacity={0.5}
        style={{
          color: !disabled ? "white" : "transparent",
        }}
      />
      {children}
    </ExtendedFAB>
  );
}
export default FAB;
